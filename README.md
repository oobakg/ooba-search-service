# ooba-search-service
This service is used as bridge between ooba-crm-api and thrid party data providers

## Installation
System requirements:
- mongodb v4.2+
- node.js v14+

```bash
# 1. Install all dependencies
npm ci

# 2. Set up env variables

# create .env file in root of the project
touch .env

# fillup using .env.example as example

# 3. Run mongodb migrations
# https://github.com/seppevs/migrate-mongo
npx migrate-mongo up
```

## Deployment
Deployment is done using **pm2** through folling command
```bash
# First of all you need pm2 to be installed globally
npm i -g pm2

# Then you need to run following script
pm2 deploy production
```