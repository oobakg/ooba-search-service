const Config = require('./lib/config')

module.exports = {
  mongodb: {
    url: Config.mongo.url,
    databaseName: Config.mongo.database,
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true
    }
  },
  migrationsDir: 'migrations',
  changelogCollectionName: 'changelog',
  migrationFileExtension: '.js',
  useFileHash: false
}
