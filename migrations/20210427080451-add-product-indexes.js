module.exports = {
  async up (db) {
    const collection = db.collection('products')

    await collection.createIndex({ marketplace: 1, weight: 1 }, { name: 'weight_marketplace_idx' })

    return Promise.resolve()
  },

  async down (db) {
    const collection = db.collection('products')

    await collection.dropIndex('weight_marketplace_idx')

    return Promise.resolve()
  }
}
