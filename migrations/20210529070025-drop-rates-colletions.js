module.exports = {
  async up (db) {
    await db.dropCollection('fx-rates')
    await db.dropCollection('shipping-rates')
    await db.dropCollection('comission-rates')
  },

  async down () {
    return Promise.resolve()
  }
}
