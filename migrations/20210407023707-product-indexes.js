module.exports = {
  async up (db) {
    const collection = db.collection('products')

    await collection.createIndex({ createdAt: 1 }, { name: 'createdAt_idx' })
    await collection.createIndex({ weight: 1 }, { name: 'weight_idx' })

    return Promise.resolve()
  },

  async down (db) {
    const collection = db.collection('products')

    await collection.dropIndex('createdAt_idx')
    await collection.dropIndex('weight_idx')

    return Promise.resolve()
  }
}
