module.exports = {
  async up (db) {
    return db.collection('categories').insertMany(
      [
        {
          externalId: '1101',
          label: 'Ноутбуки',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '1201',
          label: 'MP3',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '1205',
          label: 'Гарнитуры',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '1402',
          label: 'Цифровые видеокамеры',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '1403',
          label: 'Цифровые фотоаппараты',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '1512',
          label: 'Мобильные телефоны',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '1623',
          label: 'Юбки',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '1629',
          label: 'Блузки',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '2908',
          label: 'Зажигалки ZIPPO',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '3035',
          label: 'Брюки спортивные',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '5174',
          label: 'Музыка',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110201',
          label: 'Материнские платы',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110202',
          label: 'Память',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110203',
          label: 'Процессоры',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110204',
          label: 'Приводы для гибких дисков',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110205',
          label: 'Звуковые карты',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110206',
          label: 'Видеокарты',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110207',
          label: 'Жёсткие диски',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110209',
          label: 'Wi-Fi',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110210',
          label: 'Клавиатуры',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110211',
          label: 'Корпуса',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110212',
          label: 'DVD-привод',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110215',
          label: 'Кулеры и системы охлаждения',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110216',
          label: 'ТВ-тюнеры',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110219',
          label: 'USB to USB',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110308',
          label: 'Игровой компютер',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110501',
          label: 'Сканеры',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110502',
          label: 'ЖК-мониторы',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110507',
          label: 'Внешний жёсткий диск',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110508',
          label: 'Web-камеры',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110510',
          label: 'Медиаконвертеры',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110511',
          label: 'Графические планшеты и дигитайзеры',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110514',
          label: 'Принтеры',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110805',
          label: 'Коммутаторы (Хабы)',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110808',
          label: 'Беспроводные точки доступа',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '111202',
          label: 'Факсы',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '111204',
          label: 'МФУ',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '111219',
          label: 'Проекторы',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '111404',
          label: 'Картридж для принтера',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '111703',
          label: 'USB-модемы',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121704',
          label: 'Кабели HDMI',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '140114',
          label: 'Пленочные фотоаппараты',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '140116',
          label: 'Объективы',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '140916',
          label: 'Вспышки и аксессуары',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '150704',
          label: 'Чехлы для iPhone 6',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '150708',
          label: 'Штативы',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '162104',
          label: 'Рубашки',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '162201',
          label: 'Капри и бриджи',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '162205',
          label: 'Джинсы',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '162404',
          label: 'Спортивные костюмы',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '162701',
          label: 'Платья',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '162702',
          label: 'Платья',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '172630',
          label: 'MP3-плееры и аксессуары',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '172635',
          label: 'Принтеры',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '210211',
          label: 'Зонты',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '211509',
          label: 'Тумбы под телевизор',
          weight: 39,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '211710',
          label: 'Шредеры',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '228013',
          label: 'Инструменты',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '229534',
          label: 'Программное обеспечение',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '229575',
          label: 'ПК',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '261203',
          label: 'Трехколесные велосипеды',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '261204',
          label: 'Детские велосипеды',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '261205',
          label: 'Складные велосипеды',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '261701',
          label: 'Хлодильники',
          weight: 48,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '261703',
          label: 'Bluetooth-гарнитуры',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '261704',
          label: 'GPS-навигаторы',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '261705',
          label: 'Пылесосы',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '261710',
          label: 'FM-модуляторы',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '261712',
          label: 'Ионизаторы воздуха',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '261713',
          label: 'Сабвуферы',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '283155',
          label: 'Книги',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '284507',
          label: 'Товары для кухни',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '290901',
          label: 'Курительные трубки',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '290902',
          label: 'Портсигары',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '302909',
          label: 'Запонки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '302910',
          label: 'Бейсболки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '350301',
          label: 'Стиральные машины',
          weight: 35,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '350401',
          label: 'Кондиционеры',
          weight: 57,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '350402',
          label: 'Очистители воздуха',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '350404',
          label: 'Обогреватели',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '350502',
          label: 'Индукционные плитки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '350504',
          label: 'Фильтры и умягчители для воды',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '350507',
          label: 'Кофемашины',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '350511',
          label: 'Вытяжки',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '465600',
          label: 'Подержанные книги и учебники',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '468642',
          label: 'Видеоигры',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '471306',
          label: 'Прочие детские товары',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '495224',
          label: 'Лампы и светильники',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '502394',
          label: 'Камера и Фото',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '511228',
          label: 'Прочие бытовые товары',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '551240',
          label: 'Строительные материалы',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '565098',
          label: 'Настольные компьютеры',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '599858',
          label: 'Журналы',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '1036592',
          label: 'Одежда',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '1036700',
          label: 'Аксессуары',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '1040662',
          label: 'Детская одежда',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '1057792',
          label: 'Постельные принадлежности и ванна',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '1057794',
          label: 'Мебель и декор',
          weight: 30,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '1064954',
          label: 'Товары для офиса',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '1077068',
          label: 'Автомобильная электроника',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '3367581',
          label: 'Ювелирные изделия',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '3375251',
          label: 'Спорт и активный отдых',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '3386071',
          label: 'Fan Shop',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '3407731',
          label: 'Упражнения и Фитнес',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '3410851',
          label: 'Гольф',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '3421331',
          label: 'Хождение под парусом и',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '3754161',
          label: 'Кухня и Ванна Светильники',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '3760901',
          label: 'Товары для здоровья и гигиены',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '3760911',
          label: 'Красота',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '11091801',
          label: 'Музыкальные инструменты',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '15684181',
          label: 'Автозапчасти и аксессуары',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '15706571',
          label: 'Колеса и шины',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '15706941',
          label: 'Инструменты и оборудование',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '15743161',
          label: 'Рюкзаки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '15743631',
          label: 'Сумки и кошельки',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '16310091',
          label: 'Хозтовары',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '16310101',
          label: 'Продукты питания',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50000013',
          label: 'Тостеры',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50000360',
          label: 'Электрические одеяла',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50000436',
          label: 'Футболки хлопковые',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50000563',
          label: 'Полотенца',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50000565',
          label: 'Занавески для ванны',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50000583',
          label: 'Коврики для ванной',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50000671',
          label: 'Блузки',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50000697',
          label: 'Свитеры / Свитшоты',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50000852',
          label: 'Одежда для пожилых',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50001399',
          label: 'Хранение инвентаря',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50001403',
          label: 'Столы',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50001709',
          label: 'Чайные столики',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50001727',
          label: 'Раскладные стулья',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50001810',
          label: 'Колонки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50001813',
          label: 'Домашние кинотеатры',
          weight: 80,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002263',
          label: 'Керамические чайники',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002411',
          label: 'Ванны',
          weight: 150,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002415',
          label: 'Клавиатура + мышь',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002506',
          label: 'Формы керамические',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002535',
          label: 'Йогуртницы',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002702',
          label: 'Подкраска царапин',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002777',
          label: 'Подушки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002798',
          label: 'Салатники',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002805',
          label: 'Горшки керамические',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002807',
          label: 'Мантницы',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002809',
          label: 'Микроволновые печи',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002893',
          label: 'Диспенсеры',
          weight: 8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002894',
          label: 'Духовки',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002896',
          label: 'Измельчители отходов',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002922',
          label: 'Усилители',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002925',
          label: 'Аудио провода',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50003318',
          label: 'Микрофоны',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50003329',
          label: 'Хранение CD/DVD',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50003333',
          label: 'Райзера',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50003464',
          label: 'Тостеры',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50003477',
          label: 'Аксессуары для фотоаппаратов',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50003561',
          label: 'Биде',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50003678',
          label: 'Мундштуки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50003695',
          label: 'Электрочайники',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50003773',
          label: 'Зеркальные фотокамеры',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50003795',
          label: 'Динамики',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50003796',
          label: 'Усилители',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50003802',
          label: 'Патч-корды',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50003823',
          label: 'Столы для ноутбуков',
          weight: 38,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50003848',
          label: 'Блоки питания',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50003849',
          label: 'Источники бесперебойного питания UPS',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50003881',
          label: 'Холодильники',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50003987',
          label: 'Фритюрницы',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50004235',
          label: 'Антисон',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50004363',
          label: 'Блинницы',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50004399',
          label: 'Миксеры',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50004407',
          label: 'Ключницы',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50004436',
          label: 'Коврики для макарун',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50005009',
          label: 'DVD',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50005027',
          label: 'Ленты',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50005050',
          label: 'Bluetooth-гарнитуры',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50005055',
          label: 'Хранение игрушек',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50005058',
          label: 'Вешалки для одежды',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50005118',
          label: 'Противоскользящие ленты',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50005718',
          label: 'Системы охлаждения',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50005928',
          label: 'Посудомоечные машины',
          weight: 30,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50005953',
          label: 'Булавки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50005960',
          label: 'Слинги',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50005961',
          label: 'Сумки для мам',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50005964',
          label: 'Стульчики для кормления',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50005965',
          label: 'Кроватки-качели',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50005969',
          label: 'Огранечители дверей',
          weight: 0.05,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006010',
          label: 'Послеродовая коррекция тела',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006015',
          label: 'Молокоотсосы',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006024',
          label: 'Горшки и сиденья',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006076',
          label: 'Велосипеды',
          weight: 22,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006077',
          label: 'Детские электромобили',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006078',
          label: 'Трехколесные велосипеды',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006079',
          label: 'Самокаты',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006081',
          label: 'Ходунки',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006082',
          label: 'Детские автомобили',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006254',
          label: 'Аксессуары для вспышек',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006255',
          label: 'Фоны для съемки',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006260',
          label: 'Софтбоксы',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006502',
          label: 'Смягчение углов',
          weight: 0.05,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006503',
          label: 'Заглушки в розетки',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006533',
          label: 'Шкафы для обуви',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006584',
          label: 'Колготки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006771',
          label: 'Машинка для нарезки лапши',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006839',
          label: 'Лупы',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006846',
          label: 'Колготки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006858',
          label: 'Стеллажи',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006889',
          label: 'Бутылочки термосы',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006986',
          label: 'Полотенцесушители',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006993',
          label: 'Шапки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006994',
          label: 'Шарфы',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50007005',
          label: 'Чистка носа и ушей',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50007068',
          label: 'Брюки утепленные',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50007124',
          label: 'Бюстгалтеры',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008181',
          label: 'Кабель для принтера',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008297',
          label: 'Автомагнитолы',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008332',
          label: 'Сканеры штрих-кода',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008333',
          label: 'Ламинаторы',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008334',
          label: 'Брошюровщики',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008351',
          label: 'Системные блоки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008482',
          label: 'Цифровые фоторамки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008542',
          label: 'Проводные и DECT-телефоны',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008552',
          label: 'Утюги',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008554',
          label: 'Пылесосы',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008556',
          label: 'Соковыжималки для сои',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008557',
          label: 'Вентиляторы',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008563',
          label: 'Рации',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008612',
          label: 'Другие чаи',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008681',
          label: 'Bluetooth адаптеры',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008691',
          label: 'Роликовые коньки',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008703',
          label: 'Стерилизаторы',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008759',
          label: 'Мониторы',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008881',
          label: 'Бюстгалтер с застежкой спереди',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008882',
          label: 'Белье кружевное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008883',
          label: 'Бюстгалтеры кружевные',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008884',
          label: 'Корсеты',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008888',
          label: 'Бюстгалтеры спортивные',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008889',
          label: 'Бюстгальтеры силиконовые',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008897',
          label: 'Пиджаки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008900',
          label: 'Пуховики',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008901',
          label: 'Ветровки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008904',
          label: 'Куртки кожаные',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008905',
          label: 'Шубы',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50009032',
          label: 'Подтяжки',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50009039',
          label: 'Пледы вязаные',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50009522',
          label: 'Бутылочки',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50009529',
          label: 'Посуда',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50009808',
          label: 'Тумбы под ТВ',
          weight: 40,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010158',
          label: 'Куртки и толстовки',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010159',
          label: 'Кроссовки',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010160',
          label: 'Пиджаки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010167',
          label: 'Джинсы с дырками',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010218',
          label: 'Детские коляски',
          weight: 8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010368',
          label: 'Солнцезащитные очки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010402',
          label: 'Поло',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010410',
          label: 'Перчатки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010421',
          label: 'Цветочный чай',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010518',
          label: 'Толстовки',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010519',
          label: 'Куртки',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010520',
          label: 'Пальто',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010524',
          label: 'Жилеты',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010526',
          label: 'Пуховики',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010527',
          label: 'Рубашки',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010530',
          label: 'Плащи',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010531',
          label: 'Пальто с мехом',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010537',
          label: 'Теплые костюмы',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010539',
          label: 'Трикотаж',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010540',
          label: 'Костюмы',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010548',
          label: 'Верхняя одежда',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010595',
          label: 'Автокресла',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010605',
          label: 'Серверы',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010613',
          label: 'Рабочие станции',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010616',
          label: 'Одежда для велоспорта',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010617',
          label: 'Велосипедная обувь',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010618',
          label: 'Велошлемы',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010619',
          label: 'Велоочки',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010621',
          label: 'Велосипедные сумки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010625',
          label: 'Щетки автомобильные',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010626',
          label: 'Пистолет для мойки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010665',
          label: 'Наклейки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010850',
          label: 'Одежда больших размеров',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010893',
          label: 'Двойной скотч прозрачный',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011095',
          label: 'Батареи',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011123',
          label: 'Рубашки',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011153',
          label: 'Жилеты',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011156',
          label: 'Вентиляторы',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011158',
          label: 'Ванны деревянные',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011159',
          label: 'Плащи',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011161',
          label: 'Куртки кожаные',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011167',
          label: 'Пуховики',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011277',
          label: 'Куртки и ветровки',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011617',
          label: 'Наборы для приготовления роллов',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011637',
          label: 'Лампы ксеноновые',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011726',
          label: 'Юбки спортивные',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011739',
          label: 'Олимпийки',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011743',
          label: 'Ботинки зимние',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011744',
          label: 'Кеды',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011745',
          label: 'Деловые сандали',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011746',
          label: 'Тапочки',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011827',
          label: 'ТВ-тюнеры',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011892',
          label: 'Очки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011907',
          label: 'Автосигнализации',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011975',
          label: 'Мягкие игрушки',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012010',
          label: 'Женские сумки',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012019',
          label: 'Чемоданы',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012027',
          label: 'Туфли',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012028',
          label: 'Угги с мехом',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012031',
          label: 'Обувь для баскетбола',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012032',
          label: 'Обувь на высоком каблуке',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012033',
          label: 'Шлепанцы',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012036',
          label: 'Обувь для бега',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012037',
          label: 'Для большого тенниса',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012038',
          label: 'Обувь для футбола',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012039',
          label: 'Для бадминтона',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012040',
          label: 'Для настольного тенниса',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012041',
          label: 'Для тренировок',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012042',
          label: 'Кеды цветные',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012043',
          label: 'Шузы',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012044',
          label: 'Кеды',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012047',
          label: 'Сапоги резиновые',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012068',
          label: 'Геймпады',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012075',
          label: 'Сумки из ткани',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012101',
          label: 'Сушилки',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012143',
          label: 'Сабвуферы',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012165',
          label: 'USB-флэшкарты',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012166',
          label: 'Карты памяти',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012167',
          label: 'Memory Stick',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012227',
          label: 'Подгузники',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012307',
          label: 'Мыши проводные',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012310',
          label: 'Ветровки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012320',
          label: 'Мыши беспроводные',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012322',
          label: 'Воланы',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012323',
          label: 'Ракетки',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012325',
          label: 'Одежда для тенниса',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012327',
          label: 'Леска для ракеток',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012331',
          label: 'Обувь для бадминтона',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012341',
          label: 'Кроссовки',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012342',
          label: 'Кожаная обувь',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012343',
          label: 'Кеды',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012345',
          label: 'Летняя обувь для малышей',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012346',
          label: 'Босоножки',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012347',
          label: 'Сапоги',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012348',
          label: 'Утеплённая обувь',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012349',
          label: 'Тапочки',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012351',
          label: 'Сланцы для малышей',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012353',
          label: 'Резиновые сапоги',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012357',
          label: 'Юбки',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012358',
          label: 'Свитера',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012359',
          label: 'Трикотаж',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012360',
          label: 'Верхняя одежда',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012361',
          label: 'Комплекты',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012363',
          label: 'Бандажи',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012364',
          label: 'Комбинезоны',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012365',
          label: 'Рубашки',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012366',
          label: 'Бриджи',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012369',
          label: 'Толстовки',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012371',
          label: 'Жилеты',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012406',
          label: 'Рюкзаки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012424',
          label: 'Футболки семейные',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012437',
          label: 'Уход за ногтями',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012446',
          label: 'Товары для купания',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012458',
          label: 'Круги для купания',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012494',
          label: 'Лотки для запекания',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012509',
          label: 'Насадки для крема',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012517',
          label: 'Очки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012582',
          label: 'Сумки для ноутбуков',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012652',
          label: 'Шлемы',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012656',
          label: 'Наколенники',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012657',
          label: 'Перчатки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012661',
          label: 'Брюки',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012662',
          label: 'Мотоботы',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012663',
          label: 'Багажники',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012696',
          label: 'Фотобумага',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012770',
          label: 'Куклы',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012771',
          label: 'Спальное платье',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012772',
          label: 'Пижамы',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012773',
          label: 'Халаты',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012775',
          label: 'Длинный корсет',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012777',
          label: 'Брюки утепленные',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012778',
          label: 'Белье для него и нее',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012786',
          label: 'Прокладки для груди',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012847',
          label: 'Photobox',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012863',
          label: 'Брызговики',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012864',
          label: 'Звукоизоляция',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012894',
          label: 'Накладки на дверные ручки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012895',
          label: 'Расширители колесных арок',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012898',
          label: 'Защитные пленки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012903',
          label: 'Накладки на бампер',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012906',
          label: 'Обувь для туризма',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012908',
          label: 'Обувь для рыбалки',
          weight: 1.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012935',
          label: 'Рейлинги',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012946',
          label: 'Обувь для тенниса',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012950',
          label: 'Таймеры',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013007',
          label: 'Электротермосы',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013008',
          label: 'Рисоварки',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013009',
          label: 'Скороварки',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013011',
          label: 'Кашеварки',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013023',
          label: 'Машинки для изготовления мороженого',
          weight: 80,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013027',
          label: 'Фритюрницы',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013151',
          label: 'SSD диски',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013189',
          label: 'Блузки',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013194',
          label: 'Пальто кашемировое',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013196',
          label: 'Жилеты',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013326',
          label: 'Души портативный',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013334',
          label: 'Держатели для полотенец',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013379',
          label: 'Фены промышленные',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013431',
          label: 'Крепление проводов',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013474',
          label: 'Водонагреватели',
          weight: 30,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013475',
          label: 'Водонагреватели электрические',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013497',
          label: 'Водонагреватели газовые',
          weight: 30,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013498',
          label: 'Водонагреватели солнечные',
          weight: 40,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013504',
          label: 'Провода',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013618',
          label: 'Джинсы',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013647',
          label: 'Скребок для стекол',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013659',
          label: 'Наждачная бумага',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013865',
          label: 'Колье и ожерелья',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013876',
          label: 'Броши',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013878',
          label: 'Резинки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013893',
          label: 'Раскладные стол и стулья',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013895',
          label: 'Гамаки',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013899',
          label: 'Коврики',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013933',
          label: 'Ветровки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013976',
          label: 'Зубные щетки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014075',
          label: 'Удилища',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014076',
          label: 'Крючки',
          weight: 0.05,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014077',
          label: 'Леска',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014078',
          label: 'Поплавки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014081',
          label: 'Сумки для рыбалки',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014084',
          label: 'Головные уборы для рыбалки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014087',
          label: 'Садки и куканы',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014088',
          label: 'Рыболовные ящики',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014089',
          label: 'Одежда для рыбалки',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014095',
          label: 'Катушки для спиннинга',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014097',
          label: 'Звонки для удочки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014099',
          label: 'Грузила',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014129',
          label: 'Двигатели для лодок',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014137',
          label: 'Гидрокостюмы для дайвинга и подводной охоты',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014493',
          label: 'Рюкзаки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014533',
          label: 'Защиты на двигатель',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014566',
          label: 'Тормозные колодки',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014577',
          label: 'Накладки на фары (брови)',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014611',
          label: 'Ведра автомобильные',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014614',
          label: 'Полировальные машины',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014686',
          label: 'Хромированные молдинги',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014762',
          label: 'Альпенштоки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014775',
          label: 'Фонари',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014779',
          label: 'Компасы',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014781',
          label: 'Часы туристические',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014785',
          label: 'Куртки',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014786',
          label: 'Штаны',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014788',
          label: 'Брюки флисовые',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014796',
          label: 'Термобельё',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014798',
          label: 'Пуховики спортивные',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014800',
          label: 'Куртки лыжные',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014801',
          label: 'Штаны лыжные',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014886',
          label: 'Фонари для палаток',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015112',
          label: 'Поло',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015202',
          label: 'Двухярусные кровати',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015245',
          label: 'Шизлонги',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015370',
          label: 'Перчатки и варежки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015382',
          label: 'Газовые плиты',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015397',
          label: 'Аэрогрили',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015461',
          label: 'Стулья',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015494',
          label: 'Пуфы',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015733',
          label: 'Угловые шкафы',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015740',
          label: 'Комоды',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015744',
          label: 'Шкафы для одежды',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015746',
          label: 'Шкафы',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015774',
          label: 'Мебель для гостиной',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015775',
          label: 'Мебель для столовой',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015776',
          label: 'Мебель для спальни наборы',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015778',
          label: 'Мебель для зала',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015779',
          label: 'Гардеробная',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015825',
          label: 'Детские кроватки',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015827',
          label: 'Кровати детские и подростковые',
          weight: 8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015834',
          label: 'Столы для детей',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015837',
          label: 'Детские кресла и диваны',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015916',
          label: 'Ширмы напольные',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015922',
          label: 'Винные шкафы',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015931',
          label: 'Книжные полки',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015978',
          label: 'Костюмы',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015983',
          label: 'Лыжные костюмы',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016016',
          label: 'Новогодние костюмы',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016018',
          label: 'Маски',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016084',
          label: 'Игрушки в виде животных',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016107',
          label: 'Пульты',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016143',
          label: 'Одноразовое бельё',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016191',
          label: 'Кабель оптоволоконный',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016205',
          label: 'Сетевые IP камеры',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016207',
          label: 'KVM консоли',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016213',
          label: 'ADSL-модемы',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016214',
          label: 'Шлюзы VOIP',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016235',
          label: 'Желтый чай',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016293',
          label: 'Детали из карбона',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016326',
          label: 'Диски и колпаки',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016330',
          label: 'Радио- и видеоняни',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016367',
          label: 'Вело- мото ремни безопасности',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016465',
          label: 'Варочные панели + духовка',
          weight: 8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016686',
          label: 'Белье для кормления грудью',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016688',
          label: 'Трусы',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016703',
          label: 'BMX-велосипеды',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016738',
          label: 'Купальники',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016740',
          label: 'Купальники',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016760',
          label: 'Надувная мебель',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50017072',
          label: 'Осушители воздуха',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50017188',
          label: 'Эхолоты',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50017189',
          label: 'Сигнализация',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50017576',
          label: 'Зонты для рыбалки',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50017597',
          label: 'Сумки для тенниса',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50017611',
          label: 'Сетки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50017905',
          label: 'Игровые приставки/консоли',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50017941',
          label: 'Обувь для бокса',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018103',
          label: 'Хлебопечи',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018175',
          label: 'Ножи-визитки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018218',
          label: 'Соковыжималки',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018323',
          label: 'Моноблоки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018326',
          label: 'Аксессуары для Apple',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018420',
          label: 'Нагрудники и слюнявчики',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018423',
          label: 'Воздушные шары',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018424',
          label: 'Украшение зала',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018426',
          label: 'Восковые свечи',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018429',
          label: 'Защита от дождя',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018687',
          label: 'Шоссейные велосипеды',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018691',
          label: 'Горные велосипеды',
          weight: 27,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018694',
          label: 'Двухместные / Трехместные',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018707',
          label: 'Зеркала',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018714',
          label: 'Воск',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018715',
          label: 'Очистители',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018744',
          label: 'Фонарики',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018745',
          label: 'Звонки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018746',
          label: 'Замки',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018747',
          label: 'Багажники',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018750',
          label: 'Детские сидения',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018770',
          label: 'Золотники',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018773',
          label: 'Компрессоры для мойки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018775',
          label: 'Перчатки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018778',
          label: 'Велоспидометры',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018779',
          label: 'Бутылки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018780',
          label: 'Держатели для бутылок',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018781',
          label: 'Насосы велосипедные',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018828',
          label: 'Мебель для пикника',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018834',
          label: '3D cтерео очки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018850',
          label: 'Резак бумаги',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018909',
          label: 'USB-гаджеты',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019140',
          label: 'Ботинки для альпинизма',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019202',
          label: 'Камуфляжная одежда',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019234',
          label: 'Перчатки для мойки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019270',
          label: 'Обувь для альпинизма',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019272',
          label: 'Водонепроницаемая',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019274',
          label: 'Сланцы пляжные',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019275',
          label: 'Кроссовки облегчённые',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019276',
          label: 'Обувь для скалолазов',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019278',
          label: 'Обувь для сноуборда',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019298',
          label: 'Держатели для мобильного',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019302',
          label: 'Носки для велосипедистов',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019319',
          label: 'Мини АТС',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019320',
          label: 'XDSL-оборудование',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019321',
          label: 'IP камеры',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019322',
          label: 'Wi-Fi повторители (репитеры)',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019495',
          label: 'Камеры наблюдения',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019497',
          label: 'Видеосерверы',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019515',
          label: 'Цифровые IP АТС',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019535',
          label: 'Для верховой езды',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019578',
          label: 'Палатки детские',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019581',
          label: 'Палатки для рыбалки',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019583',
          label: 'Палатки на автомобильные',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019593',
          label: 'Бинокли',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019597',
          label: 'Бинокли лазерные',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019598',
          label: 'Очки-бинокли',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019624',
          label: 'Надувные куклы',
          weight: 8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019625',
          label: 'Куклы-реалистик',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019631',
          label: 'Фаллоимитаторы',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019636',
          label: 'Стимуляторы точки G',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019637',
          label: 'Стимуляторы клитора',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019650',
          label: 'Кабели VGA',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019652',
          label: 'Комплекты с трусиками',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019655',
          label: 'Трусики',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019775',
          label: 'Гироскутеры',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019780',
          label: 'Планшеты',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019790',
          label: 'LED-телевизоры',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019813',
          label: 'Антенны',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019814',
          label: 'Блоки питания',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019936',
          label: 'Детские ночники',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019938',
          label: 'Лампы настенные',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019939',
          label: 'Торшеры',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019940',
          label: 'Комплексное освещение',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50020000',
          label: 'Кровати',
          weight: 8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50020025',
          label: 'Смесители для душа',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50020032',
          label: 'Унитазы',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50020061',
          label: 'Раковины',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50020072',
          label: 'Раковина + тумба',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50020103',
          label: 'Люстры',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50020107',
          label: 'Наружнее освещение',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50020176',
          label: 'Подшлемники',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50020262',
          label: 'Адаптеры Powerline',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50020626',
          label: 'Матрасы надувные',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50020632',
          label: 'Диваны / Кресла',
          weight: 8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50020905',
          label: 'Новогодние подарки',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50020957',
          label: 'Зеркала',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50020959',
          label: 'Зеркала для ванной',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50020960',
          label: 'Зеркала',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021005',
          label: 'Сплиттеры',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021017',
          label: 'Диммеры',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021075',
          label: 'HDD жесткие диски',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021282',
          label: 'Домкраты',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021415',
          label: 'Хлопушки',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021471',
          label: 'Молотки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021483',
          label: 'Топоры',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021816',
          label: 'Выключатели и переключатели',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021894',
          label: 'Буксировочные троса',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022226',
          label: 'Насосы компрессорные',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022246',
          label: 'Теплый пол',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022353',
          label: 'Светоотражатели',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022652',
          label: 'Лампы светодиодные',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022728',
          label: 'Костюмы спортивные',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022733',
          label: 'Детская мебель',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022762',
          label: 'GPS-трекеры',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022862',
          label: 'Крышки тормозного суппорта',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022878',
          label: 'Телевизоры и мониторы',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022879',
          label: 'Мониторы на подголовники',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022886',
          label: 'Шланг для мойки и соединения',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022887',
          label: 'Диски для полировальной машины',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022888',
          label: 'Губки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022892',
          label: 'Штаны для фитнеса',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022893',
          label: 'Одежда для фитнеса',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023096',
          label: 'Рюкзаки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023100',
          label: 'Большие сумки',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023108',
          label: 'Шорты',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023120',
          label: 'Видеорегистраторы',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023280',
          label: 'Наушники',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023320',
          label: 'Термометры',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023360',
          label: 'Журнальные столики',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023561',
          label: 'Плавки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023674',
          label: 'Разборные игрушки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023710',
          label: 'Детские скейтборды',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023730',
          label: 'Формы для печенья',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023753',
          label: 'Цветные линзы',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50024062',
          label: 'Самолеты',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50024064',
          label: 'Автомобили',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50024065',
          label: 'Радиоуправляемые вертолеты',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50024116',
          label: 'Чехлы для iPad',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50024139',
          label: 'Коннекторы',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50024676',
          label: 'Лампы настольные',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50024679',
          label: 'Светильники потолочные',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50024680',
          label: 'Лампы накаливания',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50024684',
          label: 'Декоративное освещение',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50024688',
          label: 'ПВХ обои',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50024690',
          label: 'Бумажные обои',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50024769',
          label: 'Праздничная одежда для мальчиков',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50024813',
          label: 'Прожекторы',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50024908',
          label: 'Семейные велосипеды',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50025793',
          label: 'Фейерверки',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50025804',
          label: 'Лейки для душа',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50025847',
          label: 'Тапочки',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50025852',
          label: 'Рождественские носки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50025853',
          label: 'Шапка Деда Мороза',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50025855',
          label: 'Рождественские олени',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50025857',
          label: 'Новогодние елки (украшения)',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50025912',
          label: 'Термосумки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50025922',
          label: 'Комодики для косметики',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50026194',
          label: 'Обувь резиновая',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50026552',
          label: 'Наборы для кейк-попс',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50050199',
          label: 'Сумки спортивные',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50050638',
          label: '3D Ручки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50072003',
          label: 'Фары',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50072004',
          label: 'Лобовые стекла',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50072007',
          label: 'Чехлы для мотоциклов',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50078003',
          label: 'Диски',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50080002',
          label: 'Зеркала заднего вида',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50084005',
          label: 'Наклейки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50086001',
          label: 'Приборные панели',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50088003',
          label: 'Тормозная система',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50088004',
          label: 'Воздушные фильтры',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50090001',
          label: 'Шины',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50090004',
          label: 'Ключи зажигания',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50092001',
          label: 'Свечи',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50096004',
          label: 'Крылья',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50096005',
          label: 'Сиденья',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50096006',
          label: 'Тросики',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50108005',
          label: 'Рычаги переключения передач',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50228001',
          label: 'Аудио колонки',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50228003',
          label: 'Соски',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50252001',
          label: 'Постельное белье',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50394003',
          label: 'USB Flash drive (флешки)',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50562004',
          label: 'Водоочистители',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50588001',
          label: 'Детские стульчики',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50592002',
          label: 'Обои с песком',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50650003',
          label: 'Зеркала заднего вида',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '51537011',
          label: 'Натуральные продукты',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121364004',
          label: 'Майки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121366006',
          label: 'Халаты',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121366017',
          label: 'Дизайнерская мебель',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121368033',
          label: 'Лопаты',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121370028',
          label: 'Бижутерия',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121388021',
          label: 'Визитницы',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121402020',
          label: 'Подшипники',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121408006',
          label: 'Трусы-шортики',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121412023',
          label: 'Ручки на руль',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121412049',
          label: 'Нагреватели воды',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121414017',
          label: 'Школьные пеналы',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121416008',
          label: 'Наволочки',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121420017',
          label: 'Ложки мерные',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121424009',
          label: 'Пижамы',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121434005',
          label: 'Кошельки',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121434028',
          label: 'Рычаги сцепления и тормоза',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121448020',
          label: 'Аккумуляторы',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121450017',
          label: 'Холодильники переносные / портативные',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121452038',
          label: 'Платья с длинным / коротким рукавом',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121454006',
          label: 'Часы наручные',
          weight: 0.35,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121460005',
          label: 'Трафареты силиконовые',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121462057',
          label: 'Болеро',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121466005',
          label: 'Диадемы и тиары',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121466029',
          label: 'Звуковые карты',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121470021',
          label: 'Школьные пеналы',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121472014',
          label: 'Виниловый проигрыватель',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121474012',
          label: 'Чемоданы',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121474029',
          label: 'Зарядные устройства',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121474031',
          label: 'Очистители смазки',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121476003',
          label: 'Бюстгалтеры для девочек',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121476004',
          label: 'Галстуки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121506001',
          label: 'Платья праздничные',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '122618003',
          label: 'Амортизаторы',
          weight: 100,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '122654005',
          label: 'Сумки для мужчин',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '122690003',
          label: 'Школьные рюкзаки',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '122850004',
          label: 'Навесные шкафы',
          weight: 40,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '122920001',
          label: 'Матрасы',
          weight: 30,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '123170001',
          label: 'Тестеры',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '123454001',
          label: 'Оздоровительный чай',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '123528002',
          label: 'Магниты',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '123552001',
          label: 'Сачки',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124138007',
          label: 'Шлемы виртуальной реальности VR',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124216006',
          label: 'Куртки теплые',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124230010',
          label: 'Пуховики',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124356006',
          label: 'Улун чай',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124360005',
          label: 'Черный чай',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124484016',
          label: 'Черный чай в брикетах',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124486012',
          label: 'Белый чай',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124508010',
          label: 'Водяные пистолеты',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124648001',
          label: 'Зеленый чай',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '125074041',
          label: 'Топы спортивные',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '125206002',
          label: '3D принтеры',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '125212001',
          label: 'Струйные принтеры',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '125214001',
          label: 'Принтеры',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '125216002',
          label: 'Лазерные принтеры',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '125220001',
          label: 'Фотопринтеры',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '125222001',
          label: 'Матричные принтеры',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '125224001',
          label: 'Широкоформатные принтеры',
          weight: 186,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '125320002',
          label: 'Проекторы приборной панели на стекло',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '125662001',
          label: 'Блоки для кондиционеров',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '126050001',
          label: 'Одеяла шелковые',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '126054001',
          label: 'Одеяла',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '163856011',
          label: 'MP3-музыка',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '165793011',
          label: 'Игрушки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '165796011',
          label: 'Для самых маленьких',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '193870011',
          label: 'Комплектующие',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '328182011',
          label: 'Инструменты для сада',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '346333011',
          label: 'Мотозапчасти и аксессуары',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '368395011',
          label: 'Аудиокниги',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '377110011',
          label: 'Часы',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '667846011',
          label: 'Главная Аудио & Театр',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '672123011',
          label: 'Обувь',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '706809011',
          label: 'Командные виды спорта',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '706814011',
          label: 'Отдыха на свежем воздухе',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '979455011',
          label: 'Электронные игры',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '1231241412',
          label: 'Гавно',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '1266092011',
          label: 'Телевизоры и Видео',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '1286228011',
          label: 'Электронные книги для Kindle',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '2147483647',
          label: 'Спортиваня одежда',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121386034',
          label: 'Шкатул',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121394006',
          label: 'Термобелье',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124200008',
          label: 'Шубы',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '162401',
          label: 'Деловые костюмы',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50007003',
          label: 'Шарфы шелковые',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011801',
          label: 'Ёмкости для хранения молока',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012420',
          label: 'Подушки для беременных и кормящих',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012440',
          label: 'Машинки для стрижки волос',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013869',
          label: 'Swarowski',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013875',
          label: 'Браслеты с кольцом',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014849',
          label: 'Прикроватные ограждения',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016222',
          label: 'Многофункциональные замки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016743',
          label: 'Пляжные платья и парео',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018805',
          label: 'Пеленальные столики и кроватки',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023169',
          label: 'Косметички',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '105417',
          label: 'Халаты',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '105418',
          label: 'Униформа скаутов',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '105419',
          label: 'Сорочки',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '105471',
          label: 'Свадебная обувь',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '105508',
          label: 'Одежда и аксессуары для мужчин',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '105520',
          label: 'Мужские костюмы',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '106127',
          label: 'Свадебные аксессуары',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '11452',
          label: 'Одежда для мальчиков',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '11462',
          label: 'Одежда для девочек',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '131474',
          label: 'Наряды для мам',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '131476',
          label: 'Женские шляпки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '152345',
          label: 'Танцевальная одежда для подростков',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '155198',
          label: 'Одежда для самых маленьких',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '155202',
          label: 'Обувь Unisex',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '155344',
          label: 'Аксессуары',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '15628',
          label: 'Аксессуары для самых маленьких',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '15720',
          label: 'Свадебные платья',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '163149',
          label: 'Другое',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '163525',
          label: 'Форменные брюки и шорты',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '163526',
          label: 'Форменные головные уборы',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '163528',
          label: 'Другое',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '172007',
          label: 'Танцевальная одежда для малышей',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '175628',
          label: 'Фартуки',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '175629',
          label: 'Форменные куртки и жилеты',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '175630',
          label: 'Рубашки',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '175632',
          label: 'Прочие наряды',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '175633',
          label: 'Платья для подружек невесты',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '175638',
          label: 'Танцевальная одежда',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '175639',
          label: 'Танцевальная обувь',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '175648',
          label: 'Костюмы',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '175650',
          label: 'Театральные костюмы',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '178962',
          label: 'Форменные комбинезоны',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '3263',
          label: 'Платья для девочек',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012766',
          label: 'Штаны пижамные',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '57881',
          label: 'Аксессуары для мальчиков',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '57929',
          label: 'Обувь для мальчиков',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '57974',
          label: 'Обувь для девочек',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019618',
          label: 'Вагины, анусы',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121434038',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '123216004',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008898',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008340',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002085',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013037',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '162116',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012579',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121412018',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014239',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '201195801',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50017903',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015992',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '110520',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012584',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006523',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022250',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50020676',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014771',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50009211',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '120878006',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023643',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012785',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124242004',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121364007',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124440001',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124394003',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '200584005',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50004415',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50000561',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016736',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022890',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012587',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50020104',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006890',
          label: 'Разное',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015396',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015490',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021929',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50024780',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023172',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015985',
          label: 'Разное',
          weight: 0.25,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016138',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021351',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010814',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '200602001',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50024627',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013228',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012939',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '203304',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023284',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '203301',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023789',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50004195',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011130',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016737',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023326',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015984',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012825',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021129',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015325',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019241',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008555',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50025883',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016664',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124666003',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012907',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011165',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006511',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018248',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50009248',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '162403',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50017755',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006856',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50000557',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008779',
          label: 'Разное',
          weight: 3.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50050587',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121400018',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124654004',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022833',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '203302',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015454',
          label: 'Разное',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '162103',
          label: 'Свитеры, кофты',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50522003',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014787',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023619',
          label: 'Разное',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121412004',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50005003',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013468',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008899',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021192',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006522',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021446',
          label: 'Разное',
          weight: 3.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50024133',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021813',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002900',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023652',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '122088002',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50004396',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50004432',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50024103',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '125298003',
          label: 'Разное',
          weight: 1.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002711',
          label: 'Разное',
          weight: 1.1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121366007',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013908',
          label: 'Разное',
          weight: 1.4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124698009',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023167',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121408007',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50003854',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50024657',
          label: 'Разное',
          weight: 0.25,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011738',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '250804',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008903',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50017505',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002901',
          label: 'Разное',
          weight: 3.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50017458',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '125152014',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021116',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008328',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023737',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '122290002',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021069',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015709',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010465',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015377',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50001400',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012079',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023620',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124732001',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50009047',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121384022',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '122626011',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '122692006',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '122676006',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50017727',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006894',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121462025',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124086006',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121416004',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012410',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50005759',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019358',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121484044',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014773',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '251707',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015734',
          label: 'Разное',
          weight: 100,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012625',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124462006',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50124001',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018762',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019371',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018949',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121480005',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013932',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014647',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013347',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008246',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010401',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50002928',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014238',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50009183',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019052',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50025914',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013192',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50025942',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50000567',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014571',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012413',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50132002',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124486010',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016620',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121386023',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012516',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50020603',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013465',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021246',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50017070',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121434004',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50020042',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022416',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008056',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50025386',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016093',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021565',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006188',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006747',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021215',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021814',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121414024',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50008661',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019591',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016691',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013906',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019984',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50017474',
          label: 'Разное',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010796',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023642',
          label: 'Разное',
          weight: 1.1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013436',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021198',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '125192005',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50009049',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019279',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023573',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121370019',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '126496001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121414028',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013041',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50005192',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016014',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50017503',
          label: 'Разное',
          weight: 30,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50017316',
          label: 'Разное',
          weight: 100,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015482',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012720',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '122674011',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012945',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012942',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '123532001',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012627',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016759',
          label: 'Разное',
          weight: 3.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121462005',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006950',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121394007',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019355',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018948',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016453',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50019154',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121388027',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50000582',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '162703',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015811',
          label: 'Разное',
          weight: 80,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121454009',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006236',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012862',
          label: 'Разное',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50018403',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121456030',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50090009',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121396034',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012051',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013551',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50014527',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121388015',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015939',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023281',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50006770',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '125148003',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012102',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012949',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121480006',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012941',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121474002',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50017563',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124442002',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '216505',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50015458',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50011898',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013870',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50013794',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50005002',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50016732',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '122680010',
          label: 'Разное',
          weight: 8.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012457',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50020604',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50023704',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022699',
          label: 'Разное',
          weight: 75,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50017917',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50012104',
          label: 'Разное',
          weight: 1.7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50010817',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '125138007',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '124422002',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121380009',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50022889',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '121400005',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50017094',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.834Z',
          updatedAt: '2021-03-10T11:33:59.834Z'
        },
        {
          externalId: '50021379',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018689',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016605',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010614',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50050597',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022530',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '126408004',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014840',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015376',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019609',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011894',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005818',
          label: 'Разное',
          weight: 3.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012064',
          label: 'Разное',
          weight: 1.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010798',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021270',
          label: 'Разное',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017092',
          label: 'Разное',
          weight: 3.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012123',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125180006',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125164004',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50000092',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023246',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019582',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121478009',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006759',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013961',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012090',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50472005',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021933',
          label: 'Разное',
          weight: 1.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005026',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50644004',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021556',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017774',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019240',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019131',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018923',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021954',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006119',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023287',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121384005',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '162402',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011412',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015539',
          label: 'Подкаты, инструменты',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013483',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003246',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121466010',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012677',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014512',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018815',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '201160311',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024881',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121398023',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017093',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023286',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50234005',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006280',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008376',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019594',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124246007',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015940',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010259',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '350203',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122696006',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121466026',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008565',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122670003',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016598',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '126052001',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50002794',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121458010',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124130011',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021812',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021847',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005219',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011993',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011689',
          label: 'Разное',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121416006',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50002713',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017231',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121424021',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008955',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015281',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019640',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006217',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125186009',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018824',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50002927',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022501',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018814',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125176009',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '213002',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025381',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021643',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006524',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022299',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018826',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011150',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125130008',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008183',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50001748',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125162007',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50002420',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121410023',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022502',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021466',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124208011',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021522',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006489',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018872',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021397',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121703',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '261706',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019692',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017471',
          label: 'Разное',
          weight: 3.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124700009',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009189',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009190',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025810',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025811',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023728',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008739',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021108',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017535',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003479',
          label: 'Штативы, головки',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50026084',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '211703',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024636',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021925',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018699',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003338',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '127798002',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015489',
          label: 'Разное',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022654',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50642006',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121412013',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121380014',
          label: 'Разное',
          weight: 3.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012127',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008687',
          label: 'Разное',
          weight: 3.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025937',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50002930',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121480027',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017486',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023780',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121472024',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021907',
          label: 'Постеры, картины',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121452009',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012682',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003942',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005122',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021819',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125156008',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50084001',
          label: 'Прямоток, выхлопные трубы',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006897',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025860',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '123042001',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018753',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023107',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012938',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003327',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50000051',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017632',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124222008',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125190004',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011903',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018950',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006804',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006830',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50026312',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023111',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50082001',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50082003',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021536',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020835',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019147',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017473',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '123516001',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '290501',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '123550001',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012585',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014070',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003762',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021174',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122304001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124410005',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015788',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017235',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013467',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121412022',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017472',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50026651',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023631',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011914',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '350407',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022228',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021186',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014921',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020323',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008365',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017493',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012934',
          label: 'Разное',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017529',
          label: 'Разное',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016225',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50007078',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '201157615',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '201161201',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015374',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012684',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005033',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '201157802',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018758',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018765',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50007122',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024815',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124174008',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50496015',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017233',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024921',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122616006',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017562',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017491',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018756',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016845',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016342',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014836',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014837',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018221',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016152',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008799',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019649',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018080',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50004401',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003794',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50050367',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015988',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121434031',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50002045',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024147',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024142',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50050611',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124458006',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005522',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121406022',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021665',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006758',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024696',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013025',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021927',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024129',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003760',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009249',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019663',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121482018',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012774',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121450025',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122362004',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50000802',
          label: 'Разное',
          weight: 1.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50004774',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013331',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50004775',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019051',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013370',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012144',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121702',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010409',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023518',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124506010',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005230',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008829',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124534021',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023285',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022702',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021711',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024980',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '120802',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013868',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011980',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012531',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012414',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019579',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025928',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003817',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017662',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013663',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019658',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021887',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012518',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012515',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012781',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021736',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022508',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022510',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121476036',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022509',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121540009',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024945',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017477',
          label: 'Разное',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009202',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019136',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019126',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014803',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019127',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019155',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019145',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019157',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019128',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50004390',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '123508001',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008369',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023735',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003311',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025923',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013314',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50050366',
          label: 'Кабели, разъемы, переходники',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018416',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012453',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '140912',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015495',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015483',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020747',
          label: 'Разное',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021420',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121454038',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121480019',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022566',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50074002',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '203509',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015812',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015272',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015800',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008287',
          label: 'Разное',
          weight: 100,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015805',
          label: 'Разное',
          weight: 100,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011979',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021704',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015783',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '126504001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015786',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005519',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005774',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121452006',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005480',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '123534001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011041',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50026617',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018711',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016450',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '123506001',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '127218010',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50001382',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '111220',
          label: 'Разное',
          weight: 28,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019620',
          label: 'Мастурбаторы, вагины',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006226',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50001290',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122488002',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122324007',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012583',
          label: 'Сумки, чехлы',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124708025',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50001691',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50002816',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020205',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006865',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003757',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '127666019',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124570017',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023161',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006521',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006218',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50007215',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121480026',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022505',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50001248',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '331402',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121484003',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023201',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018026',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50000566',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025919',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010815',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016354',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024939',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021902',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017915',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013877',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011717',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020846',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024607',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021634',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021849',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '350213',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014845',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012624',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008159',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121384036',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024678',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013364',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '126462014',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024135',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021555',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121434020',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '127464005',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124782006',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124330005',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023513',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013827',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009580',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006534',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '350210',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013680',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021408',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124468005',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017435',
          label: 'Разное',
          weight: 42,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021439',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003369',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021495',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006219',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023143',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021663',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011869',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008109',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124392001',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020842',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121470012',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121364013',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012784',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124462002',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121404019',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014106',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011902',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021526',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009205',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013399',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016345',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019044',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021447',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50001734',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015794',
          label: 'Разное',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006996',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121410016',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014887',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017119',
          label: 'Разное',
          weight: 35,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019487',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015943',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125082033',
          label: 'Разное',
          weight: 13,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020634',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018752',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013629',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '123334002',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012658',
          label: 'Куртки, костюмы',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017118',
          label: 'Разное',
          weight: 45,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50002254',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006687',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122658003',
          label: 'Разное',
          weight: 35,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019280',
          label: 'Разное',
          weight: 1.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021681',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010593',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013367',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020788',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50007125',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017536',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023733',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017107',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013378',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013871',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016210',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020648',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50000182',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50510009',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50002803',
          label: 'Разное',
          weight: 9,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019997',
          label: 'Разное',
          weight: 45,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021928',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008558',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015323',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020633',
          label: 'Разное',
          weight: 75,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015457',
          label: 'Разное',
          weight: 75,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121474010',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50001714',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022314',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014503',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025788',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010808',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010807',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025033',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015835',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005527',
          label: 'Разное',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121478034',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006888',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122998003',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50001937',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003763',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021020',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019622',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018924',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023746',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011990',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121418013',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024999',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008951',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008356',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019621',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009051',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021497',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016739',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50516008',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121844003',
          label: 'Разное',
          weight: 0.06,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012510',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124210012',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012921',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '123334005',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023177',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50001871',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023202',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003750',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013003',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013338',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50596014',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012630',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121420004',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '110809',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020845',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50004413',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121370009',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '126970002',
          label: 'Разное',
          weight: 75,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50002804',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125088030',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009824',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014802',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125356001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015280',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014791',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017601',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016584',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023267',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50001412',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023269',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008406',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005750',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008145',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011876',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '211507',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013669',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022503',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021612',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50004684',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121406019',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '201157011',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018823',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021104',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024779',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121472018',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006223',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012654',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011718',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019628',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024217',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '123242003',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006101',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005946',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015785',
          label: 'Разное',
          weight: 75,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015784',
          label: 'Разное',
          weight: 75,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50026394',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008652',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018748',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121366018',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015600',
          label: 'Разное',
          weight: 3.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012868',
          label: 'Разное',
          weight: 1.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020656',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016436',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013103',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '251103',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023599',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '251101',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121476023',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121390012',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121396029',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011978',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012372',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021878',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009561',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121476008',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015703',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50050583',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50050581',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125098026',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018160',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009823',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019585',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019589',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013927',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '1104',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018827',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121388026',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50514005',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122966003',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '201160807',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121408042',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006782',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50001415',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020896',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013376',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124394004',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025787',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121458013',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '201159808',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018741',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017944',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017938',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50050728',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013857',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013172',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013199',
          label: 'Разное',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014079',
          label: 'Наживки, приманки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014597',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006048',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013042',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124478010',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015620',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018709',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014561',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012094',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011418',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012888',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012112',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023774',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021567',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50001385',
          label: 'Разное',
          weight: 40,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006228',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50001865',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121482007',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121414009',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121450004',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122680007',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122678005',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021949',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121458019',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018922',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010394',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50050592',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025873',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '350310',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014899',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022810',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121420030',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012865',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016288',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012580',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005266',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018820',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024141',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121414025',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121364031',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121464024',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020246',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125190006',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50002267',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021580',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005065',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018906',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023591',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025796',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021807',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013894',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013480',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121464015',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003124',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011415',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121396007',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015790',
          label: 'Разное',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015791',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50001818',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015236',
          label: 'Разное',
          weight: 40,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012628',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021842',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010042',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017128',
          label: 'Разное',
          weight: 3.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008948',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50090006',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121460017',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005935',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121476009',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019608',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024124',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121384010',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023183',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012520',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024132',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013389',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '251203',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50002808',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50088002',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124732013',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022995',
          label: 'Разное',
          weight: 4.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '127674002',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018893',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124470002',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '201162106',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015170',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125136007',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020217',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022290',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '127654017',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021112',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125214011',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121452007',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006492',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018871',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017526',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017481',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125162005',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125124009',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125184004',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125126007',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121386020',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003781',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '126438002',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020797',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013882',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010472',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013046',
          label: 'Разное',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008403',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013434',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023697',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125248015',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012442',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '127494005',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003458',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124712018',
          label: 'Разное',
          weight: 8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124146004',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021578',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011697',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124002001',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121404028',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017543',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008282',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025938',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012490',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122648002',
          label: 'Разное',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122674003',
          label: 'Разное',
          weight: 5.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006519',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121484013',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011997',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013158',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018925',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013796',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020977',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011755',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005823',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012727',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011905',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012190',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015974',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125118008',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017986',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005966',
          label: 'Разное',
          weight: 3.9,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021595',
          label: 'Разное',
          weight: 1.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021399',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021398',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121468014',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021547',
          label: 'Разное',
          weight: 1.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020561',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021924',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018179',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023093',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124488006',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024799',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023632',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023779',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50004677',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121466040',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021892',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125186007',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125120007',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50000814',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125182006',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023594',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122680008',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014774',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122762001',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013157',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012097',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012721',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013824',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012419',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013200',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016722',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025943',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122688005',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013169',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005867',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '123038001',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121396026',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024631',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '123040004',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024783',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012507',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121386014',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025956',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '127876001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020317',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125080003',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '127656003',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011857',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012139',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017497',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017489',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023044',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021955',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014799',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025361',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018920',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019151',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021028',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021727',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124232009',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005090',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '200526003',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '127402002',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012889',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018769',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122326008',
          label: 'Разное',
          weight: 450,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019470',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017973',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50050324',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018164',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121450007',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023113',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50146004',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '350201',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012776',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023175',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016661',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012581',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012603',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013984',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005011',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013668',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011859',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018706',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020810',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017906',
          label: 'PS3, XBOX, Wii',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024427',
          label: 'Разное',
          weight: 30,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012048',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012731',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005926',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025848',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023272',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003472',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018099',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023047',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021931',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013368',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018944',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015372',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50004862',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121468012',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022506',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '111403',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121472017',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50074004',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50088005',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023144',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009960',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012089',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '111214',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021649',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121474016',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121468015',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121384007',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125214012',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019125',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124208012',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020716',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010797',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010805',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012707',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012681',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009526',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012922',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50088001',
          label: 'Разное',
          weight: 1.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50000347',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023237',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023236',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015369',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023645',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121370016',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '350121',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122368002',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023325',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021026',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018418',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124208009',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018757',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012355',
          label: 'Пальто, куртки',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009531',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020770',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50007006',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013661',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005267',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50007071',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018764',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018427',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121420003',
          label: 'Разное',
          weight: 12,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006896',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50454002',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024610',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024136',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50004394',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013039',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013958',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006988',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022527',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018713',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50004889',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005031',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017097',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012659',
          label: 'Разное&',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022735',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017150',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018732',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021562',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012508',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006943',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006049',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024229',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006756',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021387',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020627',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50648002',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50656003',
          label: 'Разное',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006257',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008698',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018410',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013405',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022883',
          label: 'Разное',
          weight: 18,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005174',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008740',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012145',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011721',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121466037',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011129',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018763',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018759',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015532',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021893',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021560',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018079',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008793',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010813',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122310015',
          label: 'Разное',
          weight: 100,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125178006',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006023',
          label: 'Прыгунки, поддержка при ходьбе',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121476033',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122716008',
          label: 'Разное',
          weight: 8.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018602',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50004234',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011977',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006757',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50498004',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025837',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015128',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012632',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012626',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012636',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024146',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018085',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021214',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124668005',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122690005',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003811',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50004924',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50518006',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125074007',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005707',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125074008',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021513',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015472',
          label: 'Разное',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005737',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016670',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016669',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025945',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012718',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023274',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121370008',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021845',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011039',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006125',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023383',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016359',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022662',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021251',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024117',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121412038',
          label: 'Разное',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021676',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125190005',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121418009',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025798',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024143',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019642',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '123814005',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019647',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019643',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '123520001',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021617',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020040',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012602',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021185',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008688',
          label: 'Разное',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019998',
          label: 'Разное',
          weight: 32,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008626',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121406004',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006748',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021661',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015221',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008399',
          label: 'Разное',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023244',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023171',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023245',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024637',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008360',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022881',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020958',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012955',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '201159706',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125140004',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021846',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125180005',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023275',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013407',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008946',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125154007',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015373',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018873',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024653',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018874',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019037',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006494',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '215201',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006526',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003449',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023318',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003460',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019837',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121458028',
          label: 'Цепи, звезды',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50642005',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '123746002',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50456009',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124172011',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014808',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121464029',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125146010',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020558',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021432',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018166',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121458018',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121472003',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009834',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018921',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018936',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023106',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006525',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '123876002',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016693',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018425',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006527',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018817',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122632003',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021202',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121416015',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021034',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125120010',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018886',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125176010',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125120006',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018594',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013374',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021003',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010871',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023319',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005053',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121468019',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021870',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121410014',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018182',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008354',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008885',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011597',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016223',
          label: 'Перегородки на двери, лестницы',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124732021',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006754',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017619',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121456003',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022870',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014237',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018240',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019305',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125154008',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125146008',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009106',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024421',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008842',
          label: 'Разное',
          weight: 3.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013195',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018821',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122712001',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125136004',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020639',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010731',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015279',
          label: 'Разное',
          weight: 0.9,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023190',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023200',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023242',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021871',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021498',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011720',
          label: 'Разное',
          weight: 0.9,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50004416',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023745',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121422012',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '127458001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '201163303',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '201163502',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012820',
          label: 'Разное',
          weight: 0.9,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '350202',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009043',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121384023',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006530',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124226015',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50114004',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003453',
          label: 'Разное',
          weight: 0.35,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025203',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017138',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023714',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015965',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '123222007',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016427',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018974',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006551',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008431',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009880',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124576010',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023618',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013159',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017082',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124738021',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124852006',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021844',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009856',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022324',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023622',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013179',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005037',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121410032',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008683',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121462013',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024643',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124170016',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014848',
          label: 'Колготки, чулки',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012867',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021308',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124604001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006805',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017284',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017893',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005899',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018930',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018749',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003511',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124496004',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011674',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018089',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006265',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121480023',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015282',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017234',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017112',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '123748001',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '261707',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020778',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '127696016',
          label: 'Разное',
          weight: 100,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022407',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013341',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023699',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023725',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018409',
          label: 'Разное',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017246',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124942005',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121388028',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '280910',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015946',
          label: 'Разное',
          weight: 4.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50001393',
          label: 'Разное',
          weight: 4.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019252',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121454007',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006253',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021305',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125252006',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50026085',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011874',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023407',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012869',
          label: 'Разное',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50644005',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023289',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012860',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012896',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121458014',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '213203',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009536',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015876',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015803',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018751',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018742',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010466',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024051',
          label: 'Разное',
          weight: 11,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50638006',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021509',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009227',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018400',
          label: 'Разное',
          weight: 1.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023750',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010419',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015813',
          label: 'Разное',
          weight: 35,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125266006',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '201193305',
          label: 'Разное',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015496',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50001716',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008093',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021074',
          label: 'Разное',
          weight: 3.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018690',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021065',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012140',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '123656001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121456013',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025791',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '150701',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019290',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013675',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125118009',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013697',
          label: 'Разное',
          weight: 3.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121408039',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016624',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016589',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016601',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016600',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006995',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019466',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014847',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018917',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008335',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006250',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124698020',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018842',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021650',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021658',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021532',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012418',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022798',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009042',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015860',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121478024',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018404',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013373',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021859',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121470015',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121420038',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121382038',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121368002',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121408018',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121462002',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020745',
          label: 'Разное',
          weight: 16,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '126498025',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021059',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017598',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017502',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50050369',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008350',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122382001',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011895',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017129',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016727',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013181',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023023',
          label: 'Разное',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008949',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017907',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005264',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003213',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023446',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019541',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012622',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016647',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125026010',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012722',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003758',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50004418',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50158004',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022668',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '211708',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121410029',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016883',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017465',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50524005',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023036',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003510',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124558013',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010789',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121394023',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50007120',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121404022',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016673',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121460031',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021852',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121472031',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022653',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121468003',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50007066',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003761',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122652010',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012826',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122330002',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121402029',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125232006',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125204002',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '201201503',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '350116',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122660009',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021429',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50001283',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009226',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003463',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124738011',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125036007',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012858',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005900',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021932',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011828',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '123036002',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018407',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011042',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122366003',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006945',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011982',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005817',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008886',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '201161903',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016587',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122372003',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021427',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021406',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023090',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020566',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50068009',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009210',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015977',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016752',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015371',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021866',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121382018',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023751',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006126',
          label: 'Разное',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003509',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50506002',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024650',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122334003',
          label: 'Разное',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122680009',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016682',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121426007',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018818',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005114',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016769',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121408011',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013657',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50007167',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009037',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124234009',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50004998',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50001718',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016805',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121456021',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023122',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023052',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017577',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017596',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009562',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014822',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50011891',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50026903',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024154',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122514001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '261407',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015229',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008305',
          label: 'Разное',
          weight: 8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024943',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003241',
          label: 'Разное',
          weight: 12,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017147',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017261',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003850',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50008686',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50022232',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021747',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50015836',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121400017',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014782',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012692',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021393',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012668',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025380',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121452004',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '201155909',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '201163407',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '253904',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '126490001',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '126498001',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018771',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021306',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012717',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012725',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020802',
          label: 'Разное',
          weight: 13,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012639',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50014559',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50016665',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018761',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122626005',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024646',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50003934',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021208',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50001284',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50009979',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121390010',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '125052004',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50025918',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '201151110',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121450027',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50000626',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121398017',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013896',
          label: 'Разное',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50001406',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121382014',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013511',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121394015',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013469',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018916',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124578005',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122612005',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50023259',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50012891',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50021546',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '124222019',
          label: 'Разное',
          weight: 35,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '201159306',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '201160707',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010467',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50019045',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006277',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50006900',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50136008',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50017100',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50013396',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020653',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50005729',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50020997',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50018693',
          label: 'Разное',
          weight: 30,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50010399',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50080005',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024781',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '122352002',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024613',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '50024652',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.835Z'
        },
        {
          externalId: '121424023',
          label: 'Разное',
          weight: 30,
          createdAt: '2021-03-10T11:33:59.835Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012333',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50010607',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201141402',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020987',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50009819',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50002916',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013176',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013630',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122692001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008836',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006899',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021019',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50007017',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003484',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '140909',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016761',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121384015',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50014772',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008950',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004884',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50026887',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201157408',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013203',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013204',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50014102',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005030',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121396013',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022253',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022274',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50014564',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021422',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50011678',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008700',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021951',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022426',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50015948',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023668',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023771',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025387',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201170713',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008353',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125024006',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121404005',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121392024',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012670',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006552',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025821',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50050424',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003481',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50050621',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50024113',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021302',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004843',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018245',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005526',
          label: 'Разное',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013632',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201166601',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018718',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50019657',
          label: 'Пеньюары, сорочки',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016608',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023496',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121418011',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012421',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122998002',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025920',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50024629',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121418017',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020976',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004644',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023314',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50002797',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '123040002',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121476027',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50015322',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016076',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201173911',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121470058',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '290805',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020713',
          label: 'Разное',
          weight: 16,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005722',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013798',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50000584',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021063',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121482006',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50015878',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50009879',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50015795',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023343',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '350409',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016599',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50050678',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201169115',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '350712',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50015847',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50001732',
          label: 'Разное',
          weight: 12,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121410018',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '333002',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121370021',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '200668001',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '127318015',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50014094',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013402',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020591',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005778',
          label: 'Разное',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012755',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50009532',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008954',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022395',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016347',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50019543',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013660',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022375',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006776',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008600',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '126530011',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021016',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125140007',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013438',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018883',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50011760',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124662003',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121382032',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121474013',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125142010',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121368037',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021726',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021521',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122634006',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018088',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013388',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021265',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023117',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021657',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50024944',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025940',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021615',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018885',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50011875',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018892',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125170004',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121478032',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121450038',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016432',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020968',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017506',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012995',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125156006',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023595',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50014894',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006942',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '350706',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021437',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121366015',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50002817',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124732012',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121388016',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020895',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50010794',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50000119',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125142006',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016690',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50015610',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021170',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121448032',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121364015',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121380035',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '126496004',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50010803',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017099',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017095',
          label: 'Разное',
          weight: 30,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017096',
          label: 'Разное',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '140115',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '127926003',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50011037',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125176005',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201163405',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '200768006',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006774',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50011127',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013346',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018074',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018608',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124754014',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018891',
          label: 'Разное',
          weight: 22,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006227',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022229',
          label: 'Разное',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121474009',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020678',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013310',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021520',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023406',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50001730',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020182',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018233',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023051',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201004001',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012106',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201168902',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50000570',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50007065',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023027',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50500007',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017942',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122396008',
          label: 'Разное',
          weight: 300,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005226',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50010238',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50002265',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016194',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50019300',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016161',
          label: 'Разное',
          weight: 100,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016644',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121364032',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003800',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021187',
          label: 'Разное',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025160',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013380',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122632002',
          label: 'Разное',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003821',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006765',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017943',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '127808010',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023059',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122742003',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006855',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013339',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021743',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021746',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50010535',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021745',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025419',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005210',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121420006',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021756',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005227',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124478011',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50011991',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121386047',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50010801',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50582001',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50015242',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124468007',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '350505',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013160',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021154',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121454005',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012456',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020021',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021403',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '123524001',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012872',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50019477',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121404006',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023640',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022528',
          label: 'Разное',
          weight: 8,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121484002',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50050419',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121426008',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025859',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022315',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023439',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50650002',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005269',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006486',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018760',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121366037',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201074001',
          label: 'Разное',
          weight: 75,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025856',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023149',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50158002',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125092014',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121382021',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021504',
          label: 'Разное',
          weight: 65,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124874005',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021218',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124688012',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023315',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121478012',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121450037',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50024635',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025820',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022231',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021825',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013355',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50011337',
          label: 'Разное',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021561',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50050610',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013174',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021303',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50024063',
          label: 'Танки, военная техника',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201173913',
          label: 'Разное',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004668',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '127680026',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124486011',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018982',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121388008',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '200784005',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016763',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004887',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021040',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021096',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '251612',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021728',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018926',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023026',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50019645',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '127876005',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121382013',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006990',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025801',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016452',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018777',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017083',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006760',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003766',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023166',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201213801',
          label: 'Разное',
          weight: 8,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012076',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121406005',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022519',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50019043',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017957',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50002252',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021553',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020027',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016208',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50009118',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008953',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022529',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201241402',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121366022',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '111405',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023212',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023207',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004819',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023147',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121462009',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023268',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50024131',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50010367',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '213205',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50011883',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50010020',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '217305',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023208',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50000061',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121388007',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018243',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201247207',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018402',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50011377',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50009040',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50019301',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023310',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006859',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50011873',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021031',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022317',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013026',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012987',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013028',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121434057',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50024649',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50019858',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018919',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021537',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012134',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006678',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020971',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020980',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020981',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125338017',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125262007',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121388022',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122642005',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121464019',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125368018',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012501',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013045',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020193',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012997',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '150804',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005723',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006256',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013625',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121398007',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021219',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122366002',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021576',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121466022',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50011996',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021052',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201159608',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121410019',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50024638',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50019323',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003860',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121478031',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121476034',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121384017',
          label: 'Разное',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50011698',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50516004',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004941',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025895',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005908',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003153',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016750',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '127664019',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021436',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021424',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122712002',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121456011',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125036023',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122342003',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125048022',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50464014',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '213202',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50009186',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003137',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124482006',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013825',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016744',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018408',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121462021',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124986002',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121484014',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012383',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50019627',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50019693',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50140003',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017327',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003459',
          label: 'Разное',
          weight: 2.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023151',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006850',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50014104',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016585',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124562009',
          label: 'Разное',
          weight: 0.15,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021540',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023066',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012616',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012604',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '123174001',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50024119',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003320',
          label: 'Разное',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '127224009',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005505',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '211707',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124860003',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017271',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50014694',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50024609',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006802',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125044023',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006799',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006121',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005902',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005897',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50011036',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50652002',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022800',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50009579',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020106',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012586',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018754',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023433',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122884004',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022650',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121404018',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50014778',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '217309',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017899',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018007',
          label: 'Разное',
          weight: 0.9,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023653',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021524',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022793',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004433',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005738',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '126242002',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121386011',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004910',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017746',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025803',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012928',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '213201',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121422011',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50026403',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50011696',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012461',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016852',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124302002',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004670',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '127666017',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013350',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016695',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '3415',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022648',
          label: 'Разное',
          weight: 3.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121426014',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018157',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008839',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018822',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021624',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50010785',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021281',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013829',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201230408',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021584',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '127654006',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50014240',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012724',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50050574',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50009835',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021304',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121452008',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017940',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50009842',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50015305',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50011917',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125358002',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125238016',
          label: 'Разное',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50002812',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008527',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021633',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50014563',
          label: 'Разное',
          weight: 8,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003559',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121418032',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004892',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '127670005',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '123512002',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201168801',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012122',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005183',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50011157',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017106',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017105',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016021',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025849',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022312',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017242',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '123552002',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50522009',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50019719',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50019626',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017966',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017512',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003604',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124320001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021886',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50001392',
          label: 'Разное',
          weight: 13,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012499',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121450006',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021260',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012767',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201229322',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018776',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004699',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008106',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017490',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017487',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018399',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016383',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021614',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020416',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020364',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021872',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017367',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004431',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020172',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016645',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50010892',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50001423',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '127900004',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013353',
          label: 'Разное',
          weight: 40,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023146',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121386015',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121396047',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020979',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50015129',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016133',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50015328',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023341',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013403',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020597',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121364011',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020666',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025806',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201159005',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121422037',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50007053',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025854',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124492008',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008741',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124396002',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50001936',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012940',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008755',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50015334',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122644005',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121454008',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50014846',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50009048',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121408015',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121420023',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121456014',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50002264',
          label: 'Разное',
          weight: 3.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50011457',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004676',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008958',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121484015',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124096010',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021506',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018008',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124004001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004858',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017108',
          label: 'Разное',
          weight: 35,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021252',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013354',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016019',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006532',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125182008',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125184003',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125156005',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021465',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121400032',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50598001',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005962',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '127690007',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122350001',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013035',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50015442',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121434009',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018091',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201222138',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121368020',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50015896',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50015561',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50015564',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50026454',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121420026',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '200822006',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121474018',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121380025',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121408041',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021095',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025805',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022440',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022511',
          label: 'Разное',
          weight: 5.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023261',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '126492001',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '211508',
          label: 'Разное',
          weight: 40,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021881',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021061',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006777',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013385',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016668',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121404026',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008385',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005218',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023777',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022313',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50024858',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121424004',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016325',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50015571',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008553',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003328',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025836',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201155110',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121366031',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201157504',
          label: 'Разное',
          weight: 500,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025435',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121474019',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006529',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012960',
          label: 'Разное',
          weight: 1.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121390011',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023689',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023691',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121470011',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004841',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121408030',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121466003',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012610',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122686001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122664003',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003451',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122372004',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122350002',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121452027',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017215',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008857',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121366014',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121484012',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50011999',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50024983',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50026360',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201168604',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023626',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124502007',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005749',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023514',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121404031',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021287',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008607',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003447',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023875',
          label: 'Разное',
          weight: 8,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '127672011',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50010894',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013031',
          label: 'Разное',
          weight: 30,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125172008',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121416025',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025891',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '123514001',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023434',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017575',
          label: 'Разное',
          weight: 5.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50014092',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '123512003',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50009535',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121476014',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50050584',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50014823',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121418015',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005180',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50520004',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022393',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003280',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023218',
          label: 'Разное',
          weight: 1.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121480031',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121412034',
          label: 'Разное',
          weight: 0.05,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50002532',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021023',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012505',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021890',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020415',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013173',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008952',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023313',
          label: 'Разное',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013534',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201048003',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020665',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121468017',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50002833',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121380027',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016153',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50015437',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004678',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012475',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50024114',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50024981',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50010790',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50010792',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020904',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023266',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017939',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017930',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '126412033',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201217740',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016755',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017122',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122320003',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121408016',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50015556',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018193',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '127212012',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003242',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008146',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124720015',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021390',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018914',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125258006',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012698',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005000',
          label: 'Ноутбуки',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021742',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004421',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008957',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50009041',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017214',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '123750006',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006050',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020555',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017976',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121366039',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50009839',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50458019',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022399',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012530',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50014560',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022803',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '200918005',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012623',
          label: 'Разное',
          weight: 0.05,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021863',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012600',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021431',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022389',
          label: 'Разное',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013898',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121482014',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121424019',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012635',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017470',
          label: 'Разное',
          weight: 35,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021409',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020818',
          label: 'Разное',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006895',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017985',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022354',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008263',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '200756001',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023114',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125076027',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012890',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016818',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012612',
          label: 'Разное',
          weight: 3.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006898',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50011691',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '127864005',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201223557',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121464022',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021888',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121396035',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50014159',
          label: 'Разное',
          weight: 75,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016646',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121422013',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016586',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016588',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50009221',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '251201',
          label: 'Разное',
          weight: 100,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50010693',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012080',
          label: 'Разное',
          weight: 1.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008306',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50050642',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012131',
          label: 'Разное',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016705',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013182',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023759',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023670',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013060',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004881',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023220',
          label: 'Разное',
          weight: 1.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023173',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023352',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121396039',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121414043',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021948',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121418020',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018239',
          label: 'Разное',
          weight: 0.01,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121454016',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022300',
          label: 'Разное',
          weight: 0.05,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50002836',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012004',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124482001',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50015341',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012521',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012900',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017377',
          label: 'Разное',
          weight: 3.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022515',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50019039',
          label: 'Разное',
          weight: 0.05,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023288',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013863',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025884',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012328',
          label: 'Разное',
          weight: 0.01,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021800',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003448',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020658',
          label: 'Разное',
          weight: 3.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124702019',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50000192',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004642',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016726',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018081',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008276',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004875',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004873',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008709',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '126524005',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125280021',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004912',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025825',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201227402',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022842',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124254004',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017101',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50019555',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '200830004',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025896',
          label: 'Разное',
          weight: 0.05,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50002811',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003812',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003243',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004797',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012689',
          label: 'Разное',
          weight: 0.05,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50009845',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016081',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018159',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008324',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023087',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50002920',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122368001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012460',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50010547',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201175405',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023630',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017270',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124472006',
          label: 'Разное',
          weight: 0.9,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005920',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121402005',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023669',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121470040',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016617',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50019090',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017782',
          label: 'Разное',
          weight: 0.51,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020771',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021880',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013801',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005057',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013337',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50001733',
          label: 'Разное',
          weight: 4.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006844',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018825',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021666',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018244',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50019639',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201241505',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022686',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008877',
          label: 'Разное',
          weight: 100,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124464016',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '127682022',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '127678015',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121382033',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121404004',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201233307',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021625',
          label: 'Разное',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121414003',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201246604',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003276',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124534013',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50008841',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025941',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023801',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50019552',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121476007',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50004968',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121478035',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012618',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122754001',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006264',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50006261',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020002',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121368016',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017863',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016756',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50000062',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021056',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025813',
          label: 'Разное',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '122376001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018226',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50026550',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018622',
          label: 'Разное',
          weight: 0.01,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50234004',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50000147',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124456023',
          label: 'Разное',
          weight: 100,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50020126',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124512008',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124564005',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012759',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021348',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50021631',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022738',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005117',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50014214',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018178',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50026195',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50009864',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023740',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50003679',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018231',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50009825',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '126662001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50518002',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022344',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125368019',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '127680017',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50082007',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121404029',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013377',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50012247',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025869',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '126602006',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50016672',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50024795',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125152011',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125268007',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005513',
          label: 'Разное',
          weight: 250,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50014134',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50022428',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '124462005',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50014067',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125348020',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '125320015',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50018938',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50014069',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201230301',
          label: 'Разное',
          weight: 250,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50014050',
          label: 'Разное',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '126200633',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121368014',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50010040',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50025413',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50015608',
          label: 'Разное',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50128001',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50023517',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50013509',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '200920003',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121480008',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '127662011',
          label: 'Разное',
          weight: 35,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50002791',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '121408043',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '201030001',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50017861',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50002800',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.836Z',
          updatedAt: '2021-03-10T11:33:59.836Z'
        },
        {
          externalId: '50005502',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015380',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017981',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121408014',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '125052041',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021217',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023215',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019638',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121454053',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023180',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121452005',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020444',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122862003',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017546',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121420025',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023752',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021311',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50646004',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022853',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50648003',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022286',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016741',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124688002',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023615',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50050559',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122652007',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '2311',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '125038023',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022568',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50008649',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012107',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021582',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021539',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018816',
          label: 'Разное',
          weight: 0.01,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023248',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50005495',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012380',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022666',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122730002',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012441',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021297',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013568',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '125204012',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50002899',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50006060',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50006279',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '123018003',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50009035',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018813',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121366010',
          label: 'Разное',
          weight: 0.01,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121416014',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50014570',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023800',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50002664',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50006046',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017523',
          label: 'Разное',
          weight: 12,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50009859',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50072005',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016451',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019656',
          label: 'Чулки, колготки',
          weight: 0.05,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50108008',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019006',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121384026',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201241307',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201173509',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121148001',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012119',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50024130',
          label: 'Разное',
          weight: 0.01,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017124',
          label: 'Разное',
          weight: 40,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012589',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50008648',
          label: 'Разное',
          weight: 0.05,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201161006',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50024150',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017530',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019330',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '125152012',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50007051',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121462029',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '127722002',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '127464003',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121422028',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021067',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017564',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121936002',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022525',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201284013',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124306001',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201169316',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022748',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50024853',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50025862',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121396005',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201222404',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016675',
          label: 'Разное',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '127666008',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50025861',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50050255',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '127684027',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121480022',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50026917',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013033',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50011175',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121398014',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '125162006',
          label: 'Разное',
          weight: 0.01,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012923',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50010895',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201304009',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121400014',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017488',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50008366',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201173319',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50009826',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '350709',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122382003',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201271188',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121448008',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121392015',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124304001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124312003',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50010513',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201194201',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015875',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122694004',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121368013',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015880',
          label: 'Разное',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '123500003',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022491',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '125022013',
          label: 'Разное',
          weight: 0.05,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021662',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021527',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015165',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015164',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '125192003',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012138',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019163',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012120',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013193',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50009828',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50003764',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018083',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '127666026',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201273775',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50014945',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121406024',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018413',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121390009',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '125952002',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018943',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50011411',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '290502',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50024155',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023344',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020401',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '127694013',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '127656021',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '127682002',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121448010',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015488',
          label: 'Разное',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124966004',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50025865',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '350615',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122238009',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020028',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '127672003',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021868',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50004661',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013596',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022795',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015525',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012859',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020346',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201305707',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018613',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '125166006',
          label: 'Разное',
          weight: 0.05,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018716',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019596',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016742',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017892',
          label: 'Разное',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017378',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021092',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013631',
          label: 'Разное',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124482010',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50011901',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018034',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015152',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015168',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121404017',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015823',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201273400',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50024685',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50050147',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012697',
          label: 'Разное',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201267998',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021858',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022411',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121364046',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012049',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019724',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50003434',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021722',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50003841',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '350123',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50014080',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '123750001',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201275082',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50008890',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017445',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017447',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017504',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021718',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50014918',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012411',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012712',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124456021',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50004685',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '123244002',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022757',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '125352011',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016701',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023621',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201304907',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201304408',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121452026',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201309209',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50024611',
          label: 'Разное',
          weight: 30,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018014',
          label: 'Разное',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50004201',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201274878',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50050718',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021830',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201269889',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201309706',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201305209',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023230',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012000',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50026402',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021552',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020038',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021467',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201256304',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201309005',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201273891',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016753',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50004880',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121462008',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201301408',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50026440',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50025446',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50014258',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017457',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50010848',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017308',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023317',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121370033',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121370031',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201305120',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021012',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022381',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013238',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201248402',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50014230',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '125024039',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201306407',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201276457',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '127902008',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012598',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201022003',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50014231',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50005904',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020854',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201131201',
          label: 'Разное',
          weight: 30,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017102',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122446001',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021626',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201306507',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50005515',
          label: 'Разное',
          weight: 16,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201309910',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50010239',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013453',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124180005',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '125342003',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015594',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015624',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019526',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022011',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017609',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121472023',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50010726',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023379',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019232',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017450',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201309105',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50072008',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50025467',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201302518',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020703',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023580',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50014621',
          label: 'Разное',
          weight: 100,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201218932',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50009247',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '127224008',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121454033',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022753',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021566',
          label: 'Разное',
          weight: 8,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017438',
          label: 'Разное',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017307',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121418004',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201240501',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020813',
          label: 'Разное',
          weight: 8,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021242',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012495',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50005214',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50000509',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201309410',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019554',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50009283',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50050308',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50025867',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021613',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023337',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015166',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50001866',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201302020',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013470',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '211112',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018726',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017569',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023633',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124074007',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023061',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013633',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022487',
          label: 'Разное',
          weight: 30,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201309307',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023727',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018723',
          label: 'Разное',
          weight: 35,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012588',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019595',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50003437',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018964',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015233',
          label: 'Разное',
          weight: 12,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015918',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023014',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124252007',
          label: 'Разное',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50006483',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021225',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012927',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023803',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '123510001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020039',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121454022',
          label: 'Разное',
          weight: 100,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '123202002',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50009154',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015330',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021543',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021544',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023672',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016356',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013397',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021410',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019297',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021191',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50514004',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021417',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50008863',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017779',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020601',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '200998002',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020720',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50026520',
          label: 'Разное',
          weight: 0.07,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201229923',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201232820',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017589',
          label: 'Разное',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122714001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012609',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016046',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122616007',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016749',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50005029',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121368012',
          label: 'Разное',
          weight: 0.05,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '350208',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50442003',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019704',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50008331',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023361',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122338002',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121466031',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121416024',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018722',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012599',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012909',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012992',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021581',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012665',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013556',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124726017',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121410022',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '125076025',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016592',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022358',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122324003',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122366005',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50003450',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201310125',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201220527',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50001715',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50006862',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012638',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019299',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201249208',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122354006',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '125204011',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015210',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121418008',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020903',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50002261',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022512',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022739',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020722',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016447',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50050345',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50005016',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201304409',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50444010',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50516005',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016747',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121458026',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121384028',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018597',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023018',
          label: 'Разное',
          weight: 0.05,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201307707',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023747',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019559',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016604',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016603',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '126784401',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019038',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021066',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121478001',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124488009',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50003558',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018237',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201312704',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201302701',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50009213',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121478030',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013986',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022681',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201171302',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012904',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020718',
          label: 'Разное',
          weight: 8,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50006868',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '216506',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201283914',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013597',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50011676',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122674001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '350212',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '123222004',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201131601',
          label: 'Разное',
          weight: 6,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020070',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121448015',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021261',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013466',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021243',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021623',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50050635',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50050625',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020973',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50011695',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201159707',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121386012',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201306908',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121398016',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50011173',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020605',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018223',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '123058001',
          label: 'Разное',
          weight: 30,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121420037',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50005910',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012643',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122842005',
          label: 'Разное',
          weight: 10,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124408002',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201302802',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50025885',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50072006',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50092003',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121474030',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50011915',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201275386',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012866',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021189',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '126604009',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122254007',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50444013',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020662',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '111217',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021488',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021534',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019556',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020668',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50025924',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121462042',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50004878',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '123326004',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022736',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121466015',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022409',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50011882',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50006939',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121396004',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121482022',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023228',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021070',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016446',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121412007',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '127484004',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50014677',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50010356',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '123520002',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023369',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121418010',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50008248',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018703',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019507',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '350406',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020748',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013562',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013426',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50024120',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201302210',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021412',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021479',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021232',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023094',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '126412023',
          label: 'Разное',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021818',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201267697',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201142502',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50011257',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50025795',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012732',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '127680016',
          label: 'Разное',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50000346',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '125080002',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121466014',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121466030',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121398022',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201274970',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012378',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121422023',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121988001',
          label: 'Разное',
          weight: 9,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '127310021',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018737',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50026821',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124308001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016256',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201371401',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '200666001',
          label: 'Разное',
          weight: 7,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122384001',
          label: 'Разное',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018205',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013415',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021396',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50003454',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121400006',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017618',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50000150',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201222113',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015047',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50025863',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017103',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013478',
          label: 'Разное',
          weight: 0.4,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023627',
          label: 'Разное',
          weight: 1.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021864',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '150706',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016045',
          label: 'Разное',
          weight: 20,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021926',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50008798',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50024854',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023755',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50006887',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023152',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121420035',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50014140',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50008330',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '123512004',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201303507',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121394012',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121448027',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201301713',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50025418',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '126496022',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020851',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201302109',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121456004',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124358002',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018097',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50008827',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124480005',
          label: 'Разное',
          weight: 30,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124976006',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122320002',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122380002',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021564',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50012496',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50004423',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018934',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50009980',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121472005',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023639',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50004877',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201315801',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017965',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122750001',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013613',
          label: 'Разное',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015148',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50114003',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50108009',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023685',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '125322008',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '126784474',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015132',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122658007',
          label: 'Разное',
          weight: 180,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124492007',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50006861',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023392',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020366',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '200922008',
          label: 'Разное',
          weight: 31,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022682',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50006222',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50524001',
          label: 'Разное',
          weight: 1.7,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201302933',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201168503',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018973',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015262',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50510004',
          label: 'Разное',
          weight: 1.8,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50522001',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201307623',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022311',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50002898',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201271077',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015864',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50024614',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015659',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '125134003',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021593',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50011733',
          label: 'Разное',
          weight: 9.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022360',
          label: 'Разное',
          weight: 15,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022371',
          label: 'Разное',
          weight: 30,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50024794',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201302510',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020174',
          label: 'Разное',
          weight: 0.53,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '125026016',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017236',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022524',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121466019',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201370701',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '123338005',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201808',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121420009',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016488',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50003494',
          label: 'Разное',
          weight: 0.6,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50000014',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '127902004',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021309',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50518003',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201276456',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020823',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016716',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201305828',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015248',
          label: 'Разное',
          weight: 50,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013216',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201312504',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021478',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50600002',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021923',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019720',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019999',
          label: 'Разное',
          weight: 100,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50050383',
          label: 'Разное',
          weight: 0.7,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022276',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019842',
          label: 'Разное',
          weight: 2.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121480011',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017436',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121406032',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021500',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021254',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121418027',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121386019',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017524',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124530008',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021233',
          label: 'Разное',
          weight: 0.8,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122680002',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023012',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50008317',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121434022',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '125104001',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121986001',
          label: 'Разное',
          weight: 40,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020801',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020807',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50018247',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121460028',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017379',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50006989',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201323801',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50021460',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50006122',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013335',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023393',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50003560',
          label: 'Разное',
          weight: 25,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121482029',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121458004',
          label: 'Разное',
          weight: 85,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121462038',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124480002',
          label: 'Разное',
          weight: 26,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50017466',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016638',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50016484',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020640',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50025921',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50020664',
          label: 'Разное',
          weight: 3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50015609',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '122644010',
          label: 'Разное',
          weight: 17,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '125100003',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '124040001',
          label: 'Разное',
          weight: 5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019866',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023914',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50524007',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50506011',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50592013',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50005062',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '200992001',
          label: 'Разное',
          weight: 31,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '127664007',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023309',
          label: 'Разное',
          weight: 1.6,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50019150',
          label: 'Разное',
          weight: 0.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201323701',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121456024',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50011858',
          label: 'Разное',
          weight: 1.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50007012',
          label: 'Разное',
          weight: 4,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50026202',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50010422',
          label: 'Разное',
          weight: 2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '121424012',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50026474',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50006742',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013074',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50011880',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '201337502',
          label: 'Разное',
          weight: 1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50006107',
          label: 'Разное',
          weight: 0.3,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50424002',
          label: 'Разное',
          weight: 0.1,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50023316',
          label: 'Разное',
          weight: 1.8,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50022245',
          label: 'Разное',
          weight: 1.5,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        },
        {
          externalId: '50013541',
          label: 'Разное',
          weight: 0.2,
          createdAt: '2021-03-10T11:33:59.837Z',
          updatedAt: '2021-03-10T11:33:59.837Z'
        }
      ]
    )
  },

  async down (db) {
    return db.collection('categories').deleteMany()
  }
}
