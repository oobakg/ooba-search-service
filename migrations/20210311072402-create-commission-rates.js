module.exports = {
  async up (db) {
    const list = await db.listCollections({ name: 'comission-rates' }, { nameOnly: true }).toArray()

    if (list.length > 0) {
      await db.dropCollection('comission-rates')
    }

    await db.createCollection('comission-rates')
    await db.createIndex('comission-rates', { minPrice: 1, maxPrice: 1, createdAt: -1 })

    return db.collection('comission-rates').insertOne({
      rates: [
        {
          minPrice: 0,
          maxPrice: 150,
          comissionPercentage: 20
        },
        {
          minPrice: 150,
          maxPrice: 350,
          comissionPercentage: 15
        },
        {
          minPrice: 350,
          maxPrice: 700,
          comissionPercentage: 10
        },

        {
          minPrice: 700,
          maxPrice: 1e9,
          comissionPercentage: 5
        }
      ],
      createdAt: new Date(),
      updatedAt: new Date()
    })
  },

  async down (db) {
    return db.dropCollection('comission-rates')
  }
}
