module.exports = {
  async up (db) {
    const collection = db.collection('products')

    await collection.createIndex({ categoryId: 1, weight: 1 }, { name: 'category_weight_idx' })

    return Promise.resolve()
  },

  async down (db) {
    const collection = db.collection('products')

    await collection.dropIndex('category_weight_idx')

    return Promise.resolve()
  }
}
