module.exports = {
  async up (db) {
    const list = await db.listCollections({ name: 'categories' }, { nameOnly: true }).toArray()

    if (list.length > 0) {
      await db.dropCollection('categories')
    }

    await db.createCollection('categories')
    await db.createIndex('categories', { externalId: 1 }, { unique: true })

    return Promise.resolve()
  },

  async down (db) {
    return db.dropCollection('categories')
  }
}
