module.exports = {
  async up (db) {
    const list = await db.listCollections({ name: 'cache' }, { nameOnly: true }).toArray()

    if (list.length > 0) {
      await db.dropCollection('cache')
    }

    await db.createCollection('cache')
    await db.createIndex('cache', { hash: 1 }, { unique: true })

    return Promise.resolve()
  },

  async down (db) {
    return db.dropCollection('cache')
  }
}
