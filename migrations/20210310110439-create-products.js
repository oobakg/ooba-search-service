module.exports = {
  async up (db) {
    const list = await db.listCollections({ name: 'products' }, { nameOnly: true }).toArray()

    if (list.length > 0) {
      await db.dropCollection('products')
    }

    await db.createCollection('products')
    await db.createIndex('products', { marketplace: 1, externalId: 1 }, { unique: true })

    return Promise.resolve()
  },

  async down (db) {
    return db.dropCollection('products')
  }
}
