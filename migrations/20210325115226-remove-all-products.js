module.exports = {
  async up (db) {
    return db.collection('products').deleteMany()
  },

  async down () {
    return Promise.resolve()
  }
}
