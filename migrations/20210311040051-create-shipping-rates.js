module.exports = {
  async up (db) {
    const list = await db.listCollections({ name: 'shipping-rates' }, { nameOnly: true }).toArray()

    if (list.length > 0) {
      await db.dropCollection('shipping-rates')
    }

    await db.createCollection('shipping-rates')
    await db.createIndex('shipping-rates', { origin: 1, destination: 1, createdAt: -1 })

    return db.collection('shipping-rates').insertOne({
      originPlace: 'CHINA',
      destinationPlace: 'KYRGYZSTAN',
      currency: 'KGS',
      rate: 350,
      createdAt: new Date(),
      updatedAt: new Date()
    })
  },

  async down (db) {
    return db.dropCollection('shipping-rates')
  }
}
