module.exports = {
  async up (db) {
    const list = await db.listCollections({ name: 'fx-rates' }, { nameOnly: true }).toArray()

    if (list.length > 0) {
      await db.dropCollection('fx-rates')
    }

    await db.createCollection('fx-rates')
    await db.createIndex('fx-rates', { sourceCurrency: 1, targetCurrency: 1, createdAt: -1 })

    return db.collection('fx-rates').insertOne({
      sourceCurrency: 'CNY',
      targetCurrency: 'KGS',
      rate: 14,
      createdAt: new Date(),
      updatedAt: new Date()
    })
  },

  async down (db) {
    return db.dropCollection('fx-rates')
  }
}
