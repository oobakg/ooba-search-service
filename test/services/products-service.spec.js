const test = require('ava')
const { bootstrapApp, Factory } = require('../utils')

test.before(async t => {
  const ProductsService = require('../../lib/services/products-service')
  t.context.app = await bootstrapApp()
  t.context.app.decorate('productsService', new ProductsService(t.context.app.models))
})

test.after(async t => {
  await t.context.app.close()
})

test('productsService should be registered', t => {
  t.assert('productsService' in t.context.app)
})

test('list should return array of records', async t => {
  const result = await t.context.app.productsService.list()
  t.assert(typeof result === 'object' && result !== null)
  t.assert(Array.isArray(result.result))
  t.assert(typeof result.total === 'number')
})

test('list should find and return only weightless categories', async t => {
  const list = Factory.buildList('product', 12)
  const weightlessList = Factory.buildList('product', 6, { weight: 0 })

  for (const data of list) {
    await t.context.app.productsService.create(data)
  }

  for (const data of weightlessList) {
    await t.context.app.productsService.create(data)
  }

  const result = await t.context.app.productsService.list({ weightless: true })
  t.assert(typeof result === 'object' && result !== null)
  t.assert(Array.isArray(result.result))
  t.assert(result.total >= weightlessList.length)
})

test('list should find and return list matched by externalId', async t => {
  const list = Factory.buildList('product', 12)

  for (const data of list) {
    await t.context.app.productsService.create(data)
  }

  const result = await t.context.app.productsService.list({ externalId: list[0].externalId })
  t.assert(typeof result === 'object' && result !== null)
  t.assert(Array.isArray(result.result))
  t.assert(result.total === 1)
  t.like(result.result[0], list[0])
})

test('list should find and return list matched by marketplace', async t => {
  const listTaobao = Factory.buildList('product', 12, { marketplace: 'taobao' })
  const list1688 = Factory.buildList('product', 8, { marketplace: 'taobao' })

  for (const data of listTaobao) {
    await t.context.app.productsService.create(data)
  }

  for (const data of list1688) {
    await t.context.app.productsService.create(data)
  }

  let result = await t.context.app.productsService.list({ marketplace: 'taobao' })
  t.assert(typeof result === 'object' && result !== null)
  t.assert(Array.isArray(result.result))
  t.assert(result.total >= listTaobao.length)

  result = await t.context.app.productsService.list({ marketplace: '1688' })
  t.assert(typeof result === 'object' && result !== null)
  t.assert(Array.isArray(result.result))
  t.assert(result.total >= list1688.length)
})

test('create should create and return new record', async t => {
  const data = Factory.build('product')
  const item = await t.context.app.productsService.create(data)

  t.assert(typeof item._id.toString() === 'string')
  t.like(item, data)
})

test('updateWeight should update weight prop and return existing record', async t => {
  const newData = Factory.build('product', { marketplace: 'taobao' })
  const newDoc = await t.context.app.productsService.create(newData)

  const newWeight = 999
  const item = await t.context.app.productsService.updateWeight(newDoc.externalId, newWeight)

  t.assert(typeof item._id.toString() === 'string')
  t.is(item.weight, newWeight)
})

test('updateChinaShippingCNY should update weight prop and return existing record', async t => {
  const newData = Factory.build('product')
  const newDoc = await t.context.app.productsService.create(newData)

  const chinaShippingCNY = 123
  const item = await t.context.app.productsService.updateChinaShippingCNY(newDoc.id, chinaShippingCNY)

  t.assert(typeof item._id.toString() === 'string')
  t.is(item.chinaShippingCNY, chinaShippingCNY)
})

test('remove should remove existing record', async t => {
  const newData = Factory.build('product')
  const newDoc = await t.context.app.productsService.create(newData)

  const result = await t.context.app.productsService.remove(newDoc.id)
  t.assert(result !== null)
})

test('findById should find and return existing record by id', async t => {
  const newData = Factory.build('product')
  const newDoc = await t.context.app.productsService.create(newData)

  const item = await t.context.app.productsService.findById(newDoc._id)
  t.assert(typeof item._id.toString() === 'string')
  t.like(item, newData)
})

test('findOneByExternalId should find and return existing record by externalId prop', async t => {
  const newData = Factory.build('product')
  const newDoc = await t.context.app.productsService.create(newData)

  const item = await t.context.app.productsService.findOneByExternalId({
    marketplace: newData.marketplace,
    externalId: newDoc.externalId
  })

  t.assert(typeof item._id.toString() === 'string')
  t.like(item, newData)
})

test('countWeightless should return number of product with weight = "0" and marketplace = "taobao"', async t => {
  const list = Factory.buildList('product', 100, { weight: 0, marketplace: 'taobao' })

  for (const data of list) {
    await t.context.app.productsService.create(data)
  }

  const result = await t.context.app.productsService.countWeightless()

  t.assert(typeof result === 'object' && result !== null)
  t.assert(typeof result.count === 'number' && result.count >= list.length)
})
