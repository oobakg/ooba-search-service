const test = require('ava')
const { bootstrapApp, Factory } = require('../utils')

test.before(async t => {
  const CategoriesService = require('../../lib/services/categories-service')
  t.context.app = await bootstrapApp()
  t.context.app.decorate('categoriesService', new CategoriesService(t.context.app.models))
})

test.after(async t => {
  await t.context.app.close()
})

test('categoriesService should be registered', t => {
  t.assert('categoriesService' in t.context.app)
})

test('list should return array of records', async t => {
  const result = await t.context.app.categoriesService.list()
  t.assert(typeof result === 'object' && result !== null)
  t.assert(Array.isArray(result.result))
  t.assert(typeof result.total === 'number')
})

test('list should find and return only weightless categories', async t => {
  const list = Factory.buildList('category', 12)
  const weightlessList = Factory.buildList('category', 6, { weight: 0 })

  for (const data of list) {
    await t.context.app.categoriesService.create(data)
  }

  for (const data of weightlessList) {
    await t.context.app.categoriesService.create(data)
  }

  const result = await t.context.app.categoriesService.list({ weightless: true })
  t.assert(typeof result === 'object' && result !== null)
  t.assert(Array.isArray(result.result))
  t.assert(result.total >= weightlessList.length)
})

test('list should find and return list matched by externalId', async t => {
  const list = Factory.buildList('category', 12)

  for (const data of list) {
    await t.context.app.categoriesService.create(data)
  }

  const result = await t.context.app.categoriesService.list({ externalId: list[0].externalId })
  t.assert(typeof result === 'object' && result !== null)
  t.assert(Array.isArray(result.result))
  t.assert(result.total === 1)
  t.like(result.result[0], list[0])
})

test('create should create and return new record', async t => {
  const data = Factory.build('category')
  const item = await t.context.app.categoriesService.create(data)

  t.assert(typeof item._id.toString() === 'string')
  t.like(item, data)
})

test('update should update and return existing record', async t => {
  const newData = Factory.build('category')
  const newDoc = await t.context.app.categoriesService.create(newData)

  const updatedData = { ...newData, label: newData.label + ' updated', weight: 1 }
  const item = await t.context.app.categoriesService.update(newDoc.id, updatedData)

  t.assert(typeof item._id.toString() === 'string')
  t.like(item, updatedData)
})

test('remove should remove existing record', async t => {
  const newData = Factory.build('category')
  const newDoc = await t.context.app.categoriesService.create(newData)

  const result = await t.context.app.categoriesService.remove(newDoc.id)
  t.assert(result !== null)
})

test('findOneByExternalId should find and return existing record by externalId prop', async t => {
  const newData = Factory.build('category')
  const newDoc = await t.context.app.categoriesService.create(newData)

  const item = await t.context.app.categoriesService.findOneByExternalId(newDoc.externalId)

  t.assert(typeof item._id.toString() === 'string')
  t.like(item, newData)
})

test('findWeightByExternalId should find category weight or create new without weight', async t => {
  const externalId = Date.now().toString()
  const weight = await t.context.app.categoriesService.findWeightByExternalId(externalId)

  const item = await t.context.app.categoriesService.findOneByExternalId(externalId)

  t.assert(typeof weight === 'number' && weight === 0)
  t.assert(typeof item._id.toString() === 'string')
  t.like(item, { externalId })
})
