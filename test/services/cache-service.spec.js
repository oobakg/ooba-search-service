const test = require('ava')
const { bootstrapApp } = require('../utils')
const CacheService = require('../../lib/services/cache-service')

test.before(async t => {
  t.context.app = await bootstrapApp()
  t.context.app.decorate('cacheService', new CacheService(t.context.app.models))
})

test.after(async t => {
  await t.context.app.close()
})

test('cacheService should be registered', t => {
  t.assert('cacheService' in t.context.app)
})

test('cacheService should set and get value by obj identifier', async t => {
  const identifier = { externalId: 123123, marketplace: 'taobao' }
  const value = { cachedValue: true }

  await t.context.app.cacheService.set(identifier, value)

  const cache = await t.context.app.cacheService.get(identifier)

  t.like(value, cache)
})
