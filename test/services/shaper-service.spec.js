const test = require('ava')
const { getProductStubs } = require('../utils')

test('shapeTaobaoProduct should format "taobao" product result properly', async (t) => {
  const shaper = require('../../lib/utils/taobao-product-shaper')
  const rawDataList = getProductStubs('taobao')

  for (const data of rawDataList) {
    const product = shaper(data)
    t.assert(product.marketplace === 'taobao')
    t.assert(typeof product.externalId === 'string')
    t.assert(typeof product.categoryId === 'string')
    t.assert(typeof product.originalProductUrl === 'string')
    t.assert(typeof product.priceCNY === 'number')
    t.assert(typeof product.descriptionHtmlStr === 'string')
    t.assert(typeof product.seller === 'object' && product.seller !== null)
    t.assert(typeof product.chinaShippingCNY === 'number')
    t.assert(Array.isArray(product.options))
    t.assert(Array.isArray(product.images))
    t.assert(Array.isArray(product.descriptionProperties))
    t.assert(Array.isArray(product.optionsPricesList))
  }
})

test('shapeAlibabaProduct should format "alibaba" product result properly', async (t) => {
  const shaper = require('../../lib/utils/alibaba-product-shaper')
  const rawDataList = getProductStubs('1688')

  for (const data of rawDataList) {
    const product = shaper(data)

    t.assert(product.marketplace === '1688')
    t.assert(typeof product.externalId === 'string')
    t.assert(typeof product.categoryId === 'string')
    t.assert(typeof product.originalProductUrl === 'string')
    t.assert(typeof product.seller === 'object' && product.seller !== null)
    t.assert(Array.isArray(product.options))
    t.assert(Array.isArray(product.images))
    t.assert(Array.isArray(product.optionsPricesList))
    t.assert(Array.isArray(product.descriptionImages))
  }
})

test('shapeTaobaoSearch should format "taobao search" result properly', async (t) => {
  const shaper = require('../../lib/utils/taobao-search-shaper')
  const rawDataList = getProductStubs('search-taobao')

  for (const data of rawDataList) {
    const result = shaper(data)

    t.assert(typeof result === 'object' && result !== null)
    t.assert(typeof result.total === 'number')
    t.assert(typeof result.total === 'number')
    t.assert(Array.isArray(result.result))

    for (const item of result.result) {
      t.assert(typeof item.externalId === 'string')
      t.assert(typeof item.categoryId === 'string')
      t.assert(typeof item.title === 'string')
      t.assert(typeof item.originalProductUrl === 'string')
      t.assert(typeof item.marketplace === 'string')
      t.assert(typeof item.priceCNY === 'number' && !isNaN(item.priceCNY))
      t.assert(typeof item.seller === 'object' && item.seller !== null)
      t.assert(Array.isArray(item.images))
    }
  }
})
