const test = require('ava')
const { bootstrapApp } = require('../utils')
const SearchService = require('../../lib/services/search-service')
const Got = require('got')

test.before(async t => {
  t.context.app = await bootstrapApp()
  t.context.app.decorate('searchService', new SearchService(Got, 'apiKey'))
})

test.after(async t => {
  await t.context.app.close()
})

test('searchService should be registered', t => {
  t.assert('searchService' in t.context.app)
})

test('searchService.parseXml should parse xml', t => {
  const xml = '<Test><Item>1</Item></Test>'
  const json = t.context.app.searchService.parseXml(xml)

  t.assert(typeof json === 'object' && json !== null)
  t.assert('Test' in json)
  t.assert('Item' in json.Test)
})

test('searchService.stringifyXml should stringify to xml', t => {
  const json = { item: 1, page: 2 }
  const xml = t.context.app.searchService.stringifyXml(json)
  t.assert(typeof xml === 'string')
})
