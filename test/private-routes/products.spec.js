const test = require('ava')
const { bootstrapFullApp } = require('../utils')

test.before(async t => {
  t.context.app = await bootstrapFullApp()
  t.context.app.register(require('../../lib/private-routes/products'), { prefix: '/products' })
  await t.context.app.after()
})

test.after(async t => {
  await t.context.app.close()
})

test('GET /products should return products in paginated format', async t => {
  const response = await t.context.app.inject({
    method: 'GET',
    url: '/products?page=1&pageSize=10'
  })

  t.is(response.statusCode, 200)

  const data = response.json()
  t.assert(typeof data === 'object' && data !== null)
  t.assert(Array.isArray(data.result))
  t.assert(typeof data.total === 'number')
})

test('GET /products/weightless should return number of product with weight equal to 0', async t => {
  const response = await t.context.app.inject({
    method: 'GET',
    url: '/products/weightless'
  })

  t.is(response.statusCode, 200)

  const data = response.json()
  t.assert(typeof data === 'object' && data !== null)
  t.assert(typeof data.count === 'number')
})

// test('POST /products should create and return new record', async t => {
//   const payload = Factory.build('product')

//   const response = await t.context.app.inject({
//     method: 'POST',
//     url: '/products',
//     body: payload
//   })

//   t.is(response.statusCode, 200)

//   const data = response.json()
//   t.assert(typeof data === 'object' && data !== null)
//   t.like(data, payload)
// })

// test('PUT /products/:id should update record by id', async t => {
//   const payload = Factory.build('product')
//   const doc = await t.context.app.productsService.create(payload)

//   const updatePayload = Factory.build('product')
//   const response = await t.context.app.inject({
//     method: 'PUT',
//     url: '/products/' + doc._id,
//     body: updatePayload
//   })

//   t.is(response.statusCode, 200)

//   const updatedDate = response.json()
//   t.assert(typeof updatedDate === 'object' && updatedDate !== null)
//   t.is(doc.id, updatedDate._id)
//   t.like(updatedDate, updatePayload)
// })

// test('DELETE /products/:id should update record by id', async t => {
//   const payload = Factory.build('product')
//   const doc = await t.context.app.productsService.create(payload)

//   const response = await t.context.app.inject({
//     method: 'DELETE',
//     url: '/products/' + doc._id
//   })

//   t.is(response.statusCode, 200)
// })
