const test = require('ava')
const { bootstrapFullApp, Factory } = require('../utils')

test.before(async t => {
  t.context.app = await bootstrapFullApp()
  t.context.app.register(require('../../lib/private-routes/categories'), { prefix: '/categories' })
  await t.context.app.after()
})

test.after(async t => {
  await t.context.app.close()
})

test('GET /categories should return categories in paginated format', async t => {
  const response = await t.context.app.inject({
    method: 'GET',
    url: '/categories?page=1&pageSize=10'
  })

  t.is(response.statusCode, 200)

  const data = response.json()
  t.assert(typeof data === 'object' && data !== null)
  t.assert(Array.isArray(data.result))
  t.assert(typeof data.total === 'number')
})

test('POST /categories should create and return new record', async t => {
  const payload = Factory.build('category')

  const response = await t.context.app.inject({
    method: 'POST',
    url: '/categories',
    body: payload
  })

  t.is(response.statusCode, 200)

  const data = response.json()
  t.assert(typeof data === 'object' && data !== null)
  t.like(data, payload)
})

test('PUT /categories/:id should update record by id', async t => {
  const payload = Factory.build('category')
  const doc = await t.context.app.categoriesService.create(payload)

  const updatePayload = Factory.build('category')
  const response = await t.context.app.inject({
    method: 'PUT',
    url: '/categories/' + doc._id,
    body: updatePayload
  })

  t.is(response.statusCode, 200)

  const updatedDate = response.json()
  t.assert(typeof updatedDate === 'object' && updatedDate !== null)
  t.is(doc.id, updatedDate._id)
  t.like(updatedDate, updatePayload)
})

test('DELETE /categories/:id should update record by id', async t => {
  const payload = Factory.build('category')
  const doc = await t.context.app.categoriesService.create(payload)

  const response = await t.context.app.inject({
    method: 'DELETE',
    url: '/categories/' + doc._id
  })

  t.is(response.statusCode, 200)
})
