/* eslint-disable quote-props */
const { MARKETPLACES } = require('../../lib/constants')
const Faker = require('faker')
const { Factory } = require('rosie')
const _ = require('lodash')

module.exports = Factory

Factory.define('category').attrs({
  externalId: () => Faker.datatype.number(1e9).toString(),
  weight: () => Faker.datatype.float(),
  label: () => Faker.lorem.sentence()
})

Factory.define('product').attrs({
  externalId: () => Faker.datatype.number(1e9).toString(),
  weight: () => Faker.datatype.float(),
  marketplace: () => _.sample(Object.values(MARKETPLACES)),
  seller: () => ({
    name: Faker.company.companyName()
  })
})
