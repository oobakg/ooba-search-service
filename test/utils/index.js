const Fastify = require('fastify')
const AutoLoad = require('fastify-autoload')
const Path = require('path')
const Fs = require('fs-extra')
const { Types } = require('mongoose')
const { MongoMemoryServer } = require('mongodb-memory-server')

exports.bootstrapApp = async () => {
  const config = {
    fastify: {
      logger: {
        level: 'warn',
        prettyPrint: true
      }
    },
    mongo: {
      url: await exports.getMongoUrl(),
      database: 'test-db'
    }
  }

  const app = Fastify(config.fastify)

  app.register(require('fastify-sensible'))

  app.register(AutoLoad, {
    dir: Path.join(__dirname, '../../lib/plugins'),
    options: config
  })

  app.register(AutoLoad, {
    dir: Path.join(__dirname, '../../lib/models')
  })

  await app.after()

  return app
}

exports.bootstrapFullApp = async () => {
  const config = {
    fastify: {
      logger: {
        level: 'warn',
        prettyPrint: true
      }
    },
    mongo: {
      url: await exports.getMongoUrl(),
      database: 'test-db'
    },
    secrets: {
      opentaoApiKey: 'secret key'
    }
  }

  const app = Fastify(config.fastify)

  app.register(require('fastify-sensible'))

  app.register(AutoLoad, {
    dir: Path.join(__dirname, '../../lib/plugins'),
    options: config
  })

  app.register(require('../../lib/models'))

  app.register(require('../../lib/services'), config)

  await app.after()

  return app
}

exports.ObjectId = Types.ObjectId

exports.Factory = require('./factory')

exports.getMongoUrl = async () => {
  const mongo = new MongoMemoryServer()
  return await mongo.getUri()
}

exports.getAuthHeaders = (app, user = {}) => {
  const data = {
    _id: exports.ObjectId(),
    username: 'username',
    fullname: 'fullname',
    role: 'ADMIN',
    ...user
  }

  const jwt = app.jwt.sign(data, { audience: 'private' })

  return { Authorization: `Bearer ${jwt}` }
}

exports.getProductStubs = (namePrefix) => {
  const list = Fs
    .readdirSync(Path.join(__dirname, './product-stubs'), { withFileTypes: true })
    .filter(dirent => !dirent.isDirectory())
    .filter(dirent => dirent.name.startsWith(namePrefix))
    .map(dirent => require(Path.join(__dirname, './product-stubs', dirent.name)))

  return list
}
