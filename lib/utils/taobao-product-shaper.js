const _ = require('lodash')

module.exports = (data = {}) => {
  const source = _.get(data, 'Result')

  const product = {}

  product.marketplace = 'taobao'
  product.externalId = _.get(source, 'Item.Id')
  product.categoryId = _.get(source, 'Item.ExternalCategoryId')
  product.originalProductUrl = _.get(source, 'Item.ExternalItemUrl')
  product.title = _.get(source, 'Item.Title')
  product.defaultPrice = _.get(source, 'Item.Price.OriginalPrice')

  product.weight = _.get(source, 'AdditionalInfo.weight', 0)
  product.chinaShippingCNY = _.get(source, 'AdditionalInfo.chinaShippingCNY', 0)

  product.seller = _formatSeller(source)
  product.options = _formatOptions(source)
  product.optionsPricesList = _formatOptionsPricesList(source)
  product.images = _formatImages(source)
  product.descriptionImages = _formatDescriptionImages(source)
  product.descriptionProperties = _formatDescriptionProperties(source)
  product.priceCNY = _getFirstOptionPrice(product, source)

  product.descriptionHtmlStr = '' // deprecated

  return product
}

function _formatSeller (source = {}) {
  return {
    name: _.get(source, 'Vendor.Name'),
    deliveryScore: _.get(source, 'Vendor.Scores.DeliveryScore'),
    serviceScore: _.get(source, 'Vendor.Scores.ServiceScore'),
    sellScore: _.get(source, 'Vendor.Scores.ItemScore'),
    rating: _.get(source, 'Vendor.Credit.Level')
  }
}

function _formatOptions (source = {}) {
  const list = _.get(source, 'Item.Attributes', []) || []

  const filtered = list.filter(item => item.IsConfigurator)

  const groupped = _.groupBy(filtered, 'Pid')

  const mapped = Object.values(groupped).map((list) => {
    const [option] = list
    return {
      pid: option.Pid,
      name: option.PropertyName,
      name_cn: option.OriginalPropertyName,
      values: list.map((value) => {
        return {
          vid: value.Vid,
          name: value.Value,
          name_cn: value.OriginalValue,
          image: value.ImageUrl,
          priceKey: `${option.Pid}:${value.Vid}`
        }
      })
    }
  })

  return mapped
}

function _formatOptionsPricesList (source = {}) {
  const priceList = _.get(source, 'Item.ConfiguredItems', []) || []
  const promotionPriceList = _.get(source, 'Item.Promotions[0].ConfiguredItems', []) || []

  const promotionsSkuIdMap = promotionPriceList.reduce((accum, item) => {
    const skuId = item.Id
    const price = item.Price.OriginalPrice

    accum[skuId] = price

    return accum
  }, {})

  const mapped = priceList.map((item) => {
    const price = {
      skuId: item.Id,
      priceCNY: parseFloat(promotionsSkuIdMap[item.Id]) || parseFloat(item.Price.OriginalPrice),
      quantity: parseInt(item.Quantity) || 0,
      propPath: item.Configurators.map(item => `${item.Pid}:${item.Vid}`).join(';')
    }

    return price
  })

  return mapped
}

function _formatImages (source = {}) {
  const list = _.get(source, 'Item.Pictures', []) || []

  const mapped = list.map(item => {
    return _.get(item, 'Url')
  })

  return mapped
}

function _formatDescriptionImages (source = {}) {
  const htmlString = _.get(source, 'Item.Description', '') || ''

  const list = htmlString.match(/(https:\/\/)\S+(.jpg)|(.jpeg)|(.png)|(.gif)/g) || []

  const filtered = list
    .filter(url => /^(https)/.test(url))
    .filter(url => !/\d{3}x\d{3}/.test(url))

  return filtered
}

function _formatDescriptionProperties (source = {}) {
  const list = _.get(source, 'Item.Attributes', []) || []

  const filtered = list.filter(item => !item.IsConfigurator)

  const mapped = filtered.map(item => {
    const property = {
      label: item.PropertyName,
      description: item.Value
    }

    return property
  })

  return mapped
}

function _getFirstOptionPrice (product = {}, source = {}) {
  const { options, optionsPricesList, defaultPrice } = product

  if (options.length > 0) {
    const key = options
      .map(option => `${option.pid}:${option.values[0].vid}`)
      .join(';')

    const item = optionsPricesList.find(item => item.propPath === key)

    if (item) {
      return item.priceCNY
    }
  }

  const promotionPrice = _.get(source, 'Item.Promotions[0].Price.OriginalPrice')
  return promotionPrice || defaultPrice
}
