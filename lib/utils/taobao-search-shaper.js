const _ = require('lodash')

module.exports = (data) => {
  const list = _.get(data, 'Result.Items.Items.Content')
  const total = _.get(data, 'Result.Items.Items.TotalCount')

  const result = []

  for (const item of list) {
    const product = {
      externalId: _.get(item, 'Id'),
      categoryId: _.get(item, 'ExternalCategoryId'),
      title: _.get(item, 'Title'),
      originalProductUrl: _.get(item, 'ExternalItemUrl'),
      priceCNY: _.get(item, 'PromotionPrice.OriginalPrice') || _.get(item, 'Price.OriginalPrice'),
      seller: { name: _.get(item, 'VendorName') },
      marketplace: 'taobao',
      images: _formatImages(item)
    }

    result.push(product)
  }

  return { result, total }
}

function _formatImages (source = {}) {
  const list = _.get(source, 'Pictures', []) || []

  const mapped = list.map(item => {
    return _.get(item, 'Url')
  })

  const uniq = _.uniq(mapped)

  return uniq
}
