const _ = require('lodash')

module.exports = (data = {}) => {
  const source = _.get(data, 'productInfo')

  const product = {}

  product.marketplace = '1688'
  product.title = _.get(source, 'subject')
  product.seller = {
    name: _.get(source, 'sellerLoginId', '')
  }
  product.originalProductUrl = `https://detail.1688.com/offer/${source.productID}.html`
  product.minOrderQuantity = _.get(data, 'saleInfo.minOrderQuantity', 1)

  product.externalId = _getExternalId(source)
  product.categoryId = _getCategoryId(source)
  product.images = _formatImages(source)
  product.descriptionImages = _formatDescriptionImages(source)
  product.priceRanges = _formatPriceRanges(source)
  product.options = _formatOptions(source)
  product.optionsPricesList = _formatOptionsPricesList(source)

  return product
}

function _getExternalId (source = {}) {
  const id = _.get(source, 'productID')
  return typeof id === 'number' ? id.toString() : id
}

function _getCategoryId (source = {}) {
  const id = _.get(source, 'categoryID')
  return typeof id === 'number' ? id.toString() : id
}

function _formatImages (source = {}) {
  const list = _.get(source, 'image.images')

  const mapped = list.map(url => `https://cbu01.alicdn.com/${url}`)

  return mapped
}

function _formatDescriptionImages (source = {}) {
  const htmlString = _.get(source, 'description', '') || ''

  const list = htmlString.match(/(https:\/\/)\S+(.jpg)|(.jpeg)|(.png)|(.gif)/g) || []

  const filtered = list
    .filter(url => /^(https)/.test(url))
    .filter(url => !/\d{3}x\d{3}/.test(url))

  return filtered
}

function _formatPriceRanges (source = {}) {
  const list = _.get(source, 'saleInfo.priceRanges', []) || []

  const mapped = list.map((item, idx, arr) => {
    const range = {
      startQuantity: item.startQuantity,
      priceCNY: item.price
    }

    const nextRange = arr[idx + 1]

    if (nextRange) {
      range.endQuantity = nextRange.startQuantity - 1
    }

    return range
  })

  return mapped
}

function _formatOptions (source = {}) {
  const list = _.get(source, 'skuInfos', []) || []

  const getOptionImageUrl = (str = '') => {
    if (!str) {
      return undefined
    }

    if (str.startsWith('https')) {
      return str
    }

    return `https://cbu01.alicdn.com/${str}`
  }

  const mapped = list
    .flatMap(item => _.get(item, 'attributes', []) || [])
    .map(item => {
      const option = {
        parentLabel: item.attributeDisplayName.trim(),
        label: item.attributeValue.trim(),
        imageUrl: getOptionImageUrl(item.skuImageUrl)
      }

      return option
    })

  const uniq = _.uniqBy(mapped, 'label')

  return uniq
}

function _formatOptionsPricesList (source = {}) {
  const list = _.get(source, 'skuInfos', []) || []

  const filtered = list.filter(option => Boolean(option.price))

  const mapped = filtered.map(option => {
    const price = {
      priceCNY: option.price,
      propPath: option.attributes.map(attribute => attribute.attributeValue.trim()).join(':')
    }

    return price
  })

  return mapped
}
