module.exports = class CategoriesService {
  constructor (models) {
    this.Category = models.Category
  }

  async list (query = {}) {
    const { page = 1, pageSize = 20, weightless, externalId } = query

    const filters = {}

    if (weightless) {
      filters.weight = 0
    }

    if (typeof externalId === 'string') {
      filters.externalId = externalId
    }

    const [total, result] = await Promise.all([
      this.Category.countDocuments(filters),
      this.Category.find(filters)
        .sort({ createdAt: -1 })
        .skip((page - 1) * pageSize)
        .limit(pageSize)
        .lean()
    ])

    return { total, result }
  }

  async create (data) {
    return await this.Category.create({
      externalId: data.externalId,
      weight: data.weight,
      label: data.label
    })
  }

  async update (id, data) {
    const options = { new: true, omitUndefined: true, lean: true }
    return await this.Category.findByIdAndUpdate(id, data, options)
  }

  async remove (id) {
    return await this.Category.findByIdAndDelete(id)
  }

  async findOneByExternalId (externalId) {
    return await this.Category.findOne({ externalId }).lean()
  }

  async findWeightByExternalId (externalId) {
    const item = await this.Category.findOne({ externalId }).lean()

    if (!item) {
      await this.Category.create({ externalId })
      return 0
    }

    return item.weight
  }
}
