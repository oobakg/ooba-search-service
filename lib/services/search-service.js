/* eslint-disable new-cap */
const _ = require('lodash')
const XmlParser = require('fast-xml-parser')
const XmlStringifier = new XmlParser.j2xParser()
const taobaoProductShaper = require('../utils/taobao-product-shaper')
const taobaoSearchShaper = require('../utils/taobao-search-shaper')
const alibabaProductShaper = require('../utils/alibaba-product-shaper')

module.exports = class SearchService {
  constructor ({ jsonClient, opentaoApiKey, categoriesService, httpErrors }) {
    this.jsonClient = jsonClient
    this.opentaoApiKey = opentaoApiKey
    this.categoriesService = categoriesService
    this.httpErrors = httpErrors
  }

  parseXml (xml) {
    return XmlParser.parse(xml)
  }

  stringifyXml (json) {
    return XmlStringifier.parse(json)
  }

  opentaoErrorHandler (response) {
    const errorCode = _.snakeCase(response.ErrorCode).toUpperCase()

    if (errorCode === 'NOT_FOUND') {
      throw this.httpErrors.notFound()
    }

    throw this.httpErrors.conflict('OTAPI_ERROR_' + errorCode)
  }

  async fetchProductShippingPriceInChina (externalId) {
    const GUANZHOU_AREA_ID = 440103
    const searchParams = {
      op: 'GetShippingDetail',
      area_id: GUANZHOU_AREA_ID,
      product_id: externalId
    }
    const url = 'https://api.caps.kg/taobao'

    const response = await this.jsonClient(url, {
      responseType: 'text',
      searchParams,
      https: {
        rejectUnauthorized: false // hack to tell that certicatate is OK
      }
    })

    const json = this.parseXml(response)

    return _.get(json, 'item_get_response.price_cn', 0) || 0
  }

  async findMany (params = {}) {
    const {
      keyword,
      page = 1,
      pageSize = 20,
      minPrice,
      maxPrice,
      categoryId
    } = params

    const url = 'http://otapi.net/service-json/BatchSearchItemsFrame'

    const searchParams = {
      framePosition: (page - 1) * pageSize,
      frameSize: pageSize,
      instanceKey: this.opentaoApiKey,
      blockList: ''
    }

    const xmlParameters = {
      SearchItemsParameters: {
        Provider: 'Taobao'
      }
    }

    if (keyword) {
      xmlParameters.SearchItemsParameters.ItemTitle = keyword
    }

    if (minPrice > 0) {
      xmlParameters.SearchItemsParameters.CurrencyCode = 'CNY'
      xmlParameters.SearchItemsParameters.MinPrice = minPrice
    }

    if (maxPrice > 0) {
      xmlParameters.SearchItemsParameters.CurrencyCode = 'CNY'
      xmlParameters.SearchItemsParameters.MaxPrice = maxPrice
    }

    if (categoryId) {
      xmlParameters.SearchItemsParameters.CategoryId = categoryId
    }

    searchParams.xmlParameters = this.stringifyXml(xmlParameters)

    const response = await this.jsonClient(url, { searchParams })

    if (response.ErrorCode.toUpperCase() !== 'OK') {
      return this.opentaoErrorHandler(response)
    }

    const { total, result } = taobaoSearchShaper(response)

    for (const item of result) {
      item.weight = await this.categoriesService.findWeightByExternalId(categoryId || item.categoryId)
    }

    return { total, result }
  }

  async findByExternalId ({ externalId, marketplace }) {
    if (marketplace === 'taobao') {
      return await this._findTaobaoProduct(externalId)
    }

    return await this._findAlibabaProduct(externalId)
  }

  async _findTaobaoProduct (externalId) {
    const url = 'http://otapi.net/service-json/BatchGetItemFullInfo'
    const searchParams = {
      instanceKey: this.opentaoApiKey,
      itemId: externalId,
      blockList: 'Vendor,Description',
      xmlRequest: ''
    }

    const response = await this.jsonClient(url, { searchParams }).catch(error => error)
    console.log({ response })
    if (response.ErrorCode.toUpperCase() !== 'OK') {
      return this.opentaoErrorHandler(response)
    }

    // Add additional information

    const [
      chinaShippingCNY = 0,
      weight = 0
    ] = await Promise.all([
      this.fetchProductShippingPriceInChina(externalId),
      this.categoriesService.findWeightByExternalId(_.get(response, 'Result.Item.ExternalCategoryId'))
    ])

    response.Result.AdditionalInfo = { weight, chinaShippingCNY }

    return taobaoProductShaper(response)
  }

  async _findAlibabaProduct (externalId) {
    const searchParams = { op: 'alibaba.agent.product.simple.get', itemId: externalId }
    const url = 'https://api.caps.kg/1688/'
    const response = await this.jsonClient.get(url, { searchParams })

    if (response.errMsg) {
      throw this.httpErrors.notFound()
    }

    return alibabaProductShaper(response)
  }
}
