module.exports = class ProductsService {
  constructor (models) {
    this.Product = models.Product
  }

  async list (query = {}) {
    const { page = 1, pageSize = 20, weightless, externalId, marketplace } = query

    const filters = {}

    if (typeof marketplace === 'string') {
      filters.marketplace = marketplace
    }

    if (weightless) {
      filters.weight = 0
    }

    if (typeof externalId === 'number' || typeof externalId === 'string') {
      filters.externalId = externalId
    }

    const projection = {
      externalId: 1,
      priceCNY: 1,
      weight: 1,
      optionsPricesList: 1,
      createdAt: 1,
      updatedAt: 1,
      marketplace: 1,
      seller: 1,
      categoryId: 1
    }

    const [total, result] = await Promise.all([
      this.Product.countDocuments(filters),
      this.Product
        .find(filters, projection)
        .sort({ createdAt: -1 })
        .skip((page - 1) * pageSize)
        .limit(pageSize)
        .lean()
    ])

    return { total, result }
  }

  async create (data) {
    return await this.Product.create(data)
  }

  async updateWeight (externalId, weight) {
    const options = { new: true, omitUndefined: true, lean: true }
    return await this.Product.findOneAndUpdate({ externalId, marketplace: 'taobao' }, { weight }, options)
  }

  async updatePrices (externalId, { priceCNY, optionsPricesList }) {
    return await this.Product.findOneAndUpdate(
      { externalId, marketplace: 'taobao' },
      { priceCNY, optionsPricesList },
      { new: true, omitUndefined: true }
    )
  }

  async updateChinaShippingCNY (id, chinaShippingCNY) {
    const options = { new: true, omitUndefined: true, lean: true }
    return await this.Product.findByIdAndUpdate(id, { chinaShippingCNY }, options)
  }

  async remove (id) {
    return await this.Product.findByIdAndDelete(id)
  }

  async upsert (product) {
    const { marketplace, externalId } = product
    const options = { upsert: true, new: true, overwrite: true, lean: true }
    return await this.Product.findOneAndUpdate({ marketplace, externalId }, product, options)
  }

  async findById (id) {
    return await this.Product.findById(id).lean()
  }

  async findOneByExternalId ({ marketplace, externalId }) {
    return await this.Product.findOne({ marketplace, externalId }).lean()
  }

  async countWeightless () {
    const count = await this.Product.countDocuments({ weight: 0, marketplace: 'taobao' })
    return { count }
  }

  async mapExternalIdsToProducts (list = []) {
    const projection = {
      externalId: 1,
      priceCNY: 1,
      weight: 1,
      chinaShippingCNY: 1,
      seller: 1,
      _id: 0
    }

    const query = {
      externalId: {
        $in: list
      }
    }

    return await this.Product.find(query, projection).lean()
  }

  async batchWeightUpdate ({ categoryId, newWeight, oldWeight }) {
    return await this.Product.updateMany(
      { categoryId, weight: oldWeight }, // find products by categoryId and old category weight
      { weight: newWeight }
    )
  }
}
