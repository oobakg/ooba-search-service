const ObjectHash = require('object-hash')
const { differenceInHours } = require('date-fns')

module.exports = class CacheService {
  constructor (models) {
    this.Cache = models.Cache
  }

  hashObject (obj) {
    const options = { algorithm: 'sha1', unorderedObjects: false, ignoreUnknown: true }
    return ObjectHash(obj, options)
  }

  async get (obj) {
    const hash = this.hashObject(obj)
    const result = await this.Cache
      .findOne({ hash }, { value: 1, timestamp: 1 })
      .sort({ timestamp: -1 })
      .lean()

    if (!result || differenceInHours(new Date(), result.timestamp) > 24) {
      return false
    }

    return result.value
  }

  async set (obj, value) {
    const hash = this.hashObject(obj)
    const timestamp = Date.now()
    const options = { upsert: true, new: true, overwrite: true, lean: true }

    return await this.Cache.findOneAndUpdate({ hash }, { hash, timestamp, value }, options)
  }
}
