const Fp = require('fastify-plugin')
const Got = require('got')

const jsonClient = Got.extend({
  responseType: 'json',
  retry: 2,
  resolveBodyOnly: true
})

// Repository
const CategoriesService = require('./categories-service')
const ProductService = require('./products-service')
const CacheService = require('./cache-service')

// Api
const SearchService = require('./search-service')

module.exports = Fp(async function register (app, options, done) {
  app.decorate('categoriesService', new CategoriesService(app.models))
  app.decorate('productsService', new ProductService(app.models))
  app.decorate('cacheService', new CacheService(app.models))
  app.decorate('searchService', new SearchService({
      jsonClient,
      opentaoApiKey: options.secrets.opentaoApiKey,
      categoriesService: app.categoriesService,
      httpErrors: app.httpErrors
    })
  )

  done()
})
