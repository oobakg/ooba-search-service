const S = require('fluent-json-schema')
const { differenceInHours } = require('date-fns')
const { MARKETPLACES } = require('../constants')

module.exports = registerRoutes
module.exports.autoPrefix = '/search'

const tags = ['private', 'search']

async function registerRoutes (app, _, done) {
  app.route({
    method: 'GET',
    url: '/',
    schema: {
      tags,
      querystring: S.object()
        .additionalProperties(false)
        .prop('page', S.number().default(1))
        .prop('pageSize', S.number().maximum(50).default(20))
        .prop('keyword', S.string())
        .prop('categoryId', S.string())
        .prop('minPrice', S.number())
        .prop('maxPrice', S.number())
        .prop('raw', S.boolean().default(false))
        .ifThen(
          S.object().prop('keyword', S.string().maxLength(0)),
          S.object().required(['categoryId'])
        )
    },
    handler: async function (request) {
      const { raw, ...query } = request.query

      let searchResult = await this.cacheService.get(query)

      if (!searchResult) {
        const [error, response] = await this.to(this.searchService.findMany(query))

        if (error) {
          this.log.error(error)
          throw error
        }

        searchResult = response

        // cache only non empty results
        if (searchResult.total > 0) {
          await this.cacheService.set(query, searchResult)
        }
      }

      return searchResult
    }
  })

  app.route({
    method: 'GET',
    url: '/:marketplace/:externalId',
    schema: {
      tags,
      querystring: S.object().prop('reset', S.boolean().default(false)),
      params: S.object()
        .prop('marketplace', S.string().enum(Object.values(MARKETPLACES)))
        .prop('externalId', S.string().pattern(/^\d+$/))
    },
    handler: async function (request) {
      const { marketplace, externalId } = request.params
      const { reset } = request.query

      let product = await this.productsService.findOneByExternalId({ marketplace, externalId })

      if (!product || oneDayPassed(product.updatedAt) || reset) {
        const [error, response] = await this.to(
          this.searchService.findByExternalId({ marketplace, externalId })
        )

        if (error) {
          this.log.error(error)
          throw error
        }

        // Case when product weight had been manually changed before
        if (product && product.weight) {
          response.weight = product.weight
        }

        product = await this.productsService.upsert(response)
      }

      // bug fix: if chinaShippingCNY is null fetch and set
      if (marketplace === MARKETPLACES.taobao && product.chinaShippingCNY === null) {
        const chinaShippingCNY = await this.searchService.fetchProductShippingPriceInChina(product.externalId)
        product = await this.productsService.updateChinaShippingCNY(product._id, chinaShippingCNY)
      }

      // remove internal implementation details
      delete product._id
      delete product.__v

      return product
    }
  })

  done()
}

function oneDayPassed (dateStr) {
  const difference = differenceInHours(new Date(), dateStr)
  return difference > 24
}
