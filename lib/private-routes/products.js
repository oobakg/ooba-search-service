const S = require('fluent-json-schema')
const { MARKETPLACES } = require('../constants')

module.exports = registerRoutes
module.exports.autoPrefix = '/products'

const tags = ['private', 'products']

async function registerRoutes (app, _, done) {
  app.route({
    method: 'GET',
    url: '/',
    schema: {
      tags,
      querystring: S.object()
        .prop('page', S.number().minimum(1))
        .prop('pageSize', S.number().minimum(1).maximum(100))
        .prop('externalId', S.anyOf([S.string(), S.number()]))
        .prop('weightless', S.boolean().default(false))
        .prop('marketplace', S.string().enum(Object.values(MARKETPLACES)))
    },
    handler: async function (request) {
      const { query } = request
      return await this.productsService.list(query)
    }
  })

  app.route({
    method: 'GET',
    url: '/weightless',
    schema: {
      tags
    },
    handler: async function () {
      return await this.productsService.countWeightless()
    }
  })

  app.route({
    method: 'PATCH',
    url: '/:marketplace/:externalId',
    schema: {
      tags,
      params: S.object()
        .prop('externalId', S.number())
        .prop('marketplace', S.string().enum(Object.values(MARKETPLACES))),
      body: S.object().prop('weight', S.number().required())
    },
    handler: async function (request) {
      const { weight } = request.body
      const { externalId } = request.params

      return await this.productsService.updateWeight(externalId, weight)
    }
  })

  app.route({
    method: 'DELETE',
    url: '/:id',
    schema: {
      tags,
      params: S.object().prop('id', S.string().minLength(24).maxLength(24))
    },
    handler: async function (request) {
      const { id } = request.params
      return await this.productsService.remove(id)
    }
  })

  app.route({
    method: 'POST',
    url: '/batch-info',
    schema: {
      tags,
      body: S.array().items(S.string())
    },
    handler: async function (request) {
      const productsList = await this.productsService.mapExternalIdsToProducts(request.body)

      const map = {}

      for (const product of productsList) {
        map[product.externalId] = product
      }

      return map
    }
  })

  done()
}
