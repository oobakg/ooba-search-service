const S = require('fluent-json-schema')

module.exports = registerRoutes
module.exports.autoPrefix = '/categories'

const tags = ['private', 'categories']

async function registerRoutes (app, _, done) {
  app.route({
    method: 'GET',
    url: '/',
    schema: {
      tags,
      querystring: S.object()
        .prop('page', S.number().minimum(1))
        .prop('pageSize', S.number().minimum(1).maximum(100))
        .prop('externalId', S.string())
        .prop('weightless', S.boolean().default(false))
    },
    handler: async function (request) {
      const { query } = request
      return await this.categoriesService.list(query)
    }
  })

  app.route({
    method: 'GET',
    url: '/:externalId',
    schema: {
      tags,
      params: S.object().prop('externalId', S.number().minimum(1))
    },
    handler: async function (request) {
      const { externalId } = request.params
      return await this.categoriesService.findOneByExternalId(externalId)
    }
  })

  app.route({
    method: 'GET',
    url: '/:externalId/products',
    schema: {
      tags,
      params: S.object().prop('externalId', S.number().minimum(1)),
      querystring: S.object()
        .additionalProperties(false)
        .prop('page', S.number().default(1))
        .prop('pageSize', S.number().maximum(50).default(20))
    },
    handler: async function (request) {
      const query = request.query
      const { externalId } = request.params

      const searchParams = { ...query, categoryId: externalId }

      let searchResult = await this.cacheService.get(searchParams)

      if (!searchResult) {
        const [error, response] = await this.to(this.searchService.findMany(searchParams))

        if (error) {
          this.log.error(error)
          throw error
        }

        searchResult = response

        // cache only non empty results
        if (searchResult.total > 0) {
          await this.cacheService.set(searchParams, searchResult)
        }
      }

      return searchResult
    }
  })

  app.route({
    method: 'POST',
    url: '/',
    schema: {
      tags,
      body: S.object()
        .prop('externalId', S.string().required())
        .prop('label', S.string())
        .prop('weight', S.number().minimum(0).default(0))
    },
    handler: async function (request) {
      const { body } = request
      return await this.categoriesService.create(body)
    }
  })

  app.route({
    method: 'PUT',
    url: '/:id',
    schema: {
      tags,
      params: S.object().prop('id', S.string().minLength(24).maxLength(24)),
      body: S.object()
        .prop('externalId', S.string().required())
        .prop('label', S.string())
        .prop('weight', S.number().minimum(0).default(0))
    },
    handler: async function (request) {
      const { id } = request.params
      const { body } = request

      const oldWeight = await this.categoriesService.findWeightByExternalId(body.externalId)

      const result = await this.categoriesService.update(id, body)

      // update products of this category with new weight
      if (oldWeight !== body.weight) {
        await this.productsService.batchWeightUpdate({
          categoryId: body.externalId,
          newWeight: body.weight,
          oldWeight: oldWeight
        })
      }

      return result
    }
  })

  app.route({
    method: 'DELETE',
    url: '/:id',
    schema: {
      tags,
      params: S.object().prop('id', S.string().minLength(24).maxLength(24))
    },
    handler: async function (request) {
      const { id } = request.params
      return await this.categoriesService.remove(id)
    }
  })

  done()
}
