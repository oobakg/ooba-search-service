const Fp = require('fastify-plugin')
const Mongoose = require('mongoose')
const ObjectId = Mongoose.Types.ObjectId

module.exports = Fp(registerMongoose)

async function registerMongoose (app, options, done) {
  await Mongoose.connect(options.mongo.url, {
    dbName: options.mongo.database,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
  })

  Mongoose.connection.on('error', (error) => {
    app.log.error(error, 'Mongoose connection error')
  })

  app.addHook('onClose', async (_, done) => {
    await Mongoose.connection.close()
    done()
  })

  const mongo = { db: Mongoose.connection, ObjectId }

  if (!app.mongo) {
    app.decorate('mongo', mongo)
  }

  done()
}
