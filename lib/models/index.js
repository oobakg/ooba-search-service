const Path = require('path')
const Fs = require('fs-extra')
const Fp = require('fastify-plugin')

module.exports = Fp(registerModels)

async function registerModels (app, _, done) {
  const models = {}

  const files = Fs
    .readdirSync(Path.join(__dirname, '.'), { withFileTypes: true })
    .filter(dirent => !dirent.isDirectory())
    .map(dirent => dirent.name)
    .filter(name => name !== 'index.js')

  for (const filename of files) {
    const path = Path.join(__dirname, '.', filename)
    const modelName = filename.replace('.js', '')
    models[modelName] = require(path)
  }

  app.decorate('models', models)

  done()
}
