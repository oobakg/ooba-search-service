const { Schema, model } = require('mongoose')
const { MARKETPLACES } = require('../constants')

const options = {
  collection: 'products',
  timestamps: true
}

const schema = new Schema({
  externalId: {
    type: String,
    required: true
  },

  marketplace: {
    type: String,
    enum: Object.values(MARKETPLACES),
    required: true
  },

  seller: {
    type: Object,
    required: true
  },

  title: {
    type: String,
    default: ''
  },

  originalProductUrl: {
    type: String,
    default: ''
  },

  priceCNY: {
    type: Number,
    default: 0
  },

  categoryId: {
    type: String,
    default: ''
  },

  weight: {
    type: Number,
    default: 0
  },

  images: {
    type: Array,
    default: []
  },

  descriptionProperties: {
    type: Array,
    default: []
  },

  descriptionHtmlStr: {
    type: String,
    default: ''
  },

  options: {
    type: Array,
    default: []
  },

  optionsPricesList: {
    type: Array,
    default: []
  },

  // new props
  descriptionImages: {
    type: Array,
    default: []
  },

  chinaShippingCNY: {
    type: Number,
    default: null
  },

  // 1688 specific props
  priceRanges: {
    type: Array,
    default: []
  },

  minOrderQuantity: {
    type: Number,
    default: 0
  }
}, options)

module.exports = model('Product', schema)
