const { Schema, model } = require('mongoose')

const options = {
  collection: 'categories',
  timestamps: true
}

const schema = new Schema({
  externalId: {
    type: String,
    required: true
  },
  label: {
    type: String,
    default: ''
  },
  weight: {
    type: Number,
    default: 0
  }
}, options)

module.exports = model('Category', schema)
