const { Schema, model } = require('mongoose')

const options = {
  collection: 'cache'
}

const schema = new Schema({
  hash: {
    type: String,
    required: true,
    unique: true
  },
  value: {
    type: Object,
    required: true
  },
  timestamp: {
    type: Number,
    default: Date.now()
  }
}, options)

module.exports = model('Cache', schema)
